/**
 * Created by Tuffy on 16/8/23.
 */
'use strict';

var express = require('express');
var router = express.Router();

var uploadService = require('../routes/service/upload-service');
var baseConfig = require('../routes/resource/base-config');

/**
 * 劳务价格上传
 */
router.post('/excel', function (req, res, next) {
    uploadService.uploadExcel(req, res, function (result, message) {
        if (message) {
            res.setHeader('Content-Type', 'text/html');
            res.send({
                success: false,
                message: message,
                data: []
            });
        } else {
            uploadService.implExcel(req, result, function (data) {
                res.setHeader('Content-Type', 'text/html');
                res.send(data);
            });
        }
    });
});

/**
 * 车型油耗价格上传
 */
router.post('/vehicleExcel', function (req, res, next) {
    uploadService.uploadExcel1(req, res, function (result, message) {
        if (message) {
            res.setHeader('Content-Type', 'text/html');
            res.send({
                success: false,
                message: message,
                data: []
            });
        } else {
            uploadService.vehicleExcel(req, result, function (data) {
                res.setHeader('Content-Type', 'text/html');
                res.send(data);
            });
        }
    });
});

/**
 * 订单OTD上传
 */
router.post('/excelotd', function (req, res, next) {
    uploadService.uploadOTDExcel(req, res, function (result, message) {
        if (message) {
            res.setHeader('Content-Type', 'text/html');
            res.send({
                success: false,
                message: message,
                data: []
            });
        } else {
            uploadService.orderOTDExcel(req, result, function (data) {
                res.setHeader('Content-Type', 'text/html');
                res.send(data);
            });
        }
    });
});

/**
 * 客户订单OTD上传
 */
router.post('/excelcustomotd', function (req, res, next) {
    uploadService.uploadCutomOTDExcel(req, res, function (result, message) {
        if (message) {
            res.setHeader('Content-Type', 'text/html');
            res.send({
                success: false,
                message: message,
                data: []
            });
        } else {
            uploadService.orderCustomOTDExcel(req, result, function (data) {
                res.setHeader('Content-Type', 'text/html');
                res.send(data);
            });
        }
    });
});


module.exports = router;