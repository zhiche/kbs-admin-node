/**
 * Created by Tuffy on 2016/11/30.
 */
'use strict';

var express = require('express');
var router = express.Router();

var baseConfig = require('./resource/base-config');
var distributionService = require('./service/distribution-sevice');
var sessionUtils = require('./utils/session-utils');

/**
 * 默认页面
 */
router.get('/', function (req, res, next) {
    res.render('login');
});

/**
 * 登出
 */
router.get('/logout', function (req, res, next) {
    sessionUtils.setBaseToken(req, null);
    sessionUtils.setToken(req, null);
    res.render('login');
});

/**
 * 登录提交
 */
router.post('/submit', function (req, res, next) {
    distributionService.login(req, res, function (result) {
        if (result.success) {
            res.render('index', {
                isCompressed: baseConfig.compressed,
                version: baseConfig.version
            });
        } else {
            res.redirect('/login');
        }
    });
});

/**
 * 重定向
 */
router.get('/submit', function (req, res, next) {
    res.redirect('/');
});

module.exports = router;