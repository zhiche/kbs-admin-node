/**
 * Created by Tuffy on 2016/11/30.
 */
'use strict';

var express = require('express');
var router = express.Router();

var distributionService = require('./service/distribution-sevice');

/**
 * 分发 get请求
 */
router.get('/', function (req, res, next) {
    distributionService.get(req, res, function (result) {
        res.send(result);
    });
});

/**
 * 分发post请求
 */
router.post('/', function (req, res, next) {
    distributionService.post(req, res, function (result) {
        res.send(result);
    });
});

module.exports = router;