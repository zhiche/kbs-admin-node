/**
 * Created by Tuffy on 16/6/7.
 */
'use strict';

angular.module('admin.services', [])

// 图片加载失败指令
.directive('errSrc', ['Settings', function (Settings) {
    return {
        link: function (scope, element, attrs) {
            element.bind('error', function () {
                if (attrs.src != attrs.errSrc) {
                    if (attrs.errSrc.indexOf('http') >= 0) {
                        attrs.$set('src', attrs.errSrc);
                    } else {
                        attrs.$set('src', Settings.Context.path + attrs.errSrc);
                    }
                }
            });
        }
    }
}])
// 自定义指令repeatFinish
.directive('repeatFinish', function () {
    return {
        link: function(scope, element, attr){
            if(scope.$last == true){
                scope.$eval(attr.repeatFinish);
            }
        }
    }
})

.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            element.bind('change', function(event){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
                //附件预览
                scope.file = (event.srcElement || event.target).files[0];
                scope.getFile(scope.file);
            });
        }
    }
}])

.factory('fileReader',  function($q, $log){
    var onLoad = function(reader, deferred, scope) {
        return function () {
            scope.$apply(function () {
                deferred.resolve(reader.result);
            });
        };
    };
    var onError = function (reader, deferred, scope) {
        return function () {
            scope.$apply(function () {
                deferred.reject(reader.result);
            });
        };
    };
    var getReader = function(deferred, scope) {
        var reader = new FileReader();
        reader.onload = onLoad(reader, deferred, scope);
        reader.onerror = onError(reader, deferred, scope);
        return reader;
    };
    var readAsDataURL = function (file, scope) {
        debugger;
        var deferred = $q.defer();
        var reader = getReader(deferred, scope);
        reader.readAsDataURL(file);
        return deferred.promise;
    };
    return {
        readAsDataUrl: readAsDataURL
    };
})
// 全局服务请求service. 调用时请调用此服务, 便于全局管理
.service('HttpService', ['$http', '$q', '$state', 'Settings', '$rootScope', function($http, $q, $state, Settings, $rootScope) {

    var baseUrl = '/distribution?';

    // 校验是否登陆
    function checkLogin(data) {
        if (typeof data == 'string' && data.indexOf('<!DOCTYPE html>') >= 0) {
            Tools.layer.alert('登录超时,请重新登录', '超时提醒', function() {
                location.href = '/login';
            });
            return false;
        }
        return true;
    };

    // 格式化getter参数
    function formatGetParams(url, params) {
        var hostName = '';
        for (var i in (params || {})) {
            var thisParams = params[i];
            if (i == 'host') {
                hostName = thisParams;
                continue;
            }
            if (url.indexOf('?') >= 0) {
                url += '&' + i + '=' + thisParams;
            } else {
                url += '?' + i + '=' + thisParams;
            }
        }
        return 'host=' + hostName + '&url=' + encodeURIComponent(url);
    };

    return {
        post:function(url, params){

            var index = layer.load(2, {
                shade: [0.3,'#F7F7F7'] //0.1透明度的白色背景
            });
            var deferred = $q.defer();
            return $http.post(Settings.Context.path + baseUrl + 'host=' + (params.host ||'') + '&url=' + url, params, {
                headers: {
                    'menu': Tools.getMenu(),
                    'angular': true
                }
            })
                .success(function(data, status, headers, config){
                    if (checkLogin(data)) {
                        deferred.resolve(data, status, headers, config);
                    } else {
                        return;
                    }
                    layer.close(index);
                })
                .error(function(data, status, headers, config){
                    deferred.reject(data, status, headers, config);
                    layer.close(index);
                });
            return deferred.promise;
        },
        get:function(url, params){
            var index = layer.load(2, {
                shade: [0.3,'#F7F7F7'] //0.1透明度的白色背景
            });
            var deferred = $q.defer();
            return $http.get(Settings.Context.path + baseUrl + formatGetParams(url, params), {
                headers: {
                    'menu': Tools.getMenu(),
                    'angular': true
                }
            })
                .success(function(data, status, headers, config){
                    if (checkLogin(data)) {
                        deferred.resolve(data, status, headers, config);
                    } else {
                        return;
                    }
                    layer.close(index);
                })
                .error(function(data, status, headers, config){
                    deferred.reject(data, status, headers, config);
                    layer.close(index);
                })
            return deferred.promise;
        }
    };
}])

.service('UserTruckService', ['HttpService', function (HttpService) {
    var url = '/usertruck';
    return {
        list: function (params) {
            var action = '/list';
            return HttpService.get(url + action, params);
        },
        audit: function (truckId, driverId, status, truckType) {
            var action = '/audit';
            return HttpService.post(url + action, {truckId: truckId, driverId: driverId, status:status, truckType: truckType});
        },
        truckDetail: function (truckId) {
            var action = '/truckDetail/' + truckId;
            return HttpService.get(url + action);
        },
        driverDetail: function (truckId) {
            var action = '/driverDetail/' + truckId;
            return HttpService.get(url + action);
        },
        loadPic: function (id) {
            var action = '/loadpic/' + id;
            return HttpService.get(url + action);
        }
    }
}])

.service('AreaService', ['HttpService', function (HttpService) {
    var url = '/area';
    return {
        loadData: function (params) {
            var action = '/loadarea';
            return HttpService.get(url + action);
        },
        loadProvince: function () {
            var action = '/loadprovince';
            return HttpService.get(url + action);
        },
        loadCity: function () {
            var action = '/loadcity';
            return HttpService.get(url + action);
        },
        loadCityByCode: function (params) {
            var action = '/loadcity/' + params;
            return HttpService.get(url + action);
        },
        loadCountyByCode: function (params) {
            var action = '/loadcounty/' + params;
            return HttpService.get(url + action);
        },
        cityRatingList: function (params) {
            var action = '/cityratinglist';
            return HttpService.post(url + action, params);
        },
        updateCityRating: function (params) {
            var action = '/updatecityrating';
            return HttpService.post(url + action, params);
        }
    }
}])

.service('OrderService', ['HttpService', function (HttpService) {
    var url = '/order';
    return {
        alreadystartList: function (params) {
            var action = '/alreadystartlist';
            return HttpService.get(url + action, params);
        },
        completedList: function (params) {
            var action = '/completedlist';
            return HttpService.get(url + action, params);
        },
        changeStatus: function(id){
            var action = '/changestatus';
            return HttpService.post(url + action, {id: id});
        },
        changeStatusOrderDone: function(id){
            var action = '/changestatusdone';
            return HttpService.post(url + action, {id: id});
        },
        orderDriverstart: function(orderId){
            var action = '/driverstart';
            return HttpService.post(url + action, {orderId: orderId});
        },
        orderDonePic: function(orderId){
            var action = '/donepic';
            return HttpService.post(url + action, {orderId: orderId});
        },
        uploadimg: function(orderId, type, key){
            var action = '/uploadimg';
            return HttpService.post(url + action, {
                orderId: orderId,
                type: type,
                key: key
            });
        },
        list: function(params){
            var action = '/list';
            return HttpService.get(url + action, params);
        },
        orderDetail: function (params) {
            var action = '/orderinfo?orderid=' + params;
            return HttpService.get(url + action);
        },
        changeOrderStatus: function (params) {
            var action = '/changeorderstatus';
            return HttpService.post(url + action, params);
        }
    }
}])

.service('CompanyService', ['HttpService', function (HttpService) {
    var url = '/company';
    return {
        list: function (params) {
            var action = '/list';
            return HttpService.get(url + action, params);
        },
        companyView: function (id) {
            var action = '/view/' + id;
            return HttpService.get(url + action);
        },
        selectCity: function (proCode) {
            var action = '/select/' + proCode;
            return HttpService.get(url + action);
        },
        saveCompany: function (params) {
            var action = '/save';
            return HttpService.post(url + action, params);
            // return HttpService.post(url + action, {companyData: companyData});
        },
        addCompany: function (params) {
            var action = '/query';
            return HttpService.get(url + action, params);
            // return HttpService.post(url + action, {companyData: companyData});
        },
        // 添加企业用户
        addCompanyUser: function(params) {
            var action = '/sign';
            return HttpService.post(url + action, params);
        },
        selectCompanyUser: function(params) {
            var action = '/selectcompanyuser';
            return HttpService.get(url + action, params);
        },
        selectCompany: function() {
            var action = '/selectcompany';
            return HttpService.get(url + action);
        },
        // 根据类型 查询所有公司
        selbycategory: function(params) {
            var action = '/selbycategory/' + params;
            return HttpService.get(url + action);
        },
        carrierList: function (params) {
            var action = '/carrier';
            return HttpService.get(url + action, params);
        }
    }
}])

.service('QiniuService', ['HttpService', function (HttpService) {
    var url = '/qiniu';
    return {
        // 获取上传凭证LOGO
        getUploadToken: function () {
            var action = '/upload/brandlogo';
            return HttpService.get(url + action);
        },
        getUploadQiniuToken: function () {
            var action = '/upload/ticket';
            return HttpService.get(url + action);
        },
        // 获取下载凭证LOGO
        getDownloadTicket: function(params) {
            var action = '/downloadbrand/ticket';
            return HttpService.get(url + action, params);
        },
        // 获取下载授权
        getDownloadQiniuTicket: function(params) {
            var action = '/download/ticket';
            return HttpService.get(url + action, params);
        }
    };
}])

.service('DistanceService', ['HttpService', function (HttpService) {
    var url = '/citydis';
    return {
        // 城市运距列表
        getList: function (params) {
            var action = '/list';
            return HttpService.get(url + action, params);
        },
        // 城市运距调整
        updateDistance: function (params) {
            var action = '/update';
            return HttpService.post(url + action, params);
        }
    };
}])

.service('AwardRecordService', ['HttpService', function (HttpService) {
    var url = '/award';
    return {
        list: function () {
            var action = '/list';
            return HttpService.get(url + action);
        }
    };
}])

.service('CarInsuranceService', ['HttpService', function (HttpService) {
    var url = '/order';
    return {
        list: function () {
            var action = '/carinsurancedetails';
            return HttpService.get(url + action);
        },
        exportCarInsurances: function (params) {
            location.href = url + "/exportinfos";
            // 测试测试测试
            // return HttpService.get("/waybill/selectbyid", params);
        }
    };
}])

.service('BannerService', ['HttpService', function (HttpService) {
    var url = '/banner';
    return {
        list: function (bannerId) {
            var action = '?bannerId=' + (bannerId || '');
            return HttpService.get(url + action);
        },
        all: function () {
            var action = '/all';
            return HttpService.get(url + action);
        },
        byId: function (id) {
            var action = '/' + id;
            return HttpService.get(url + action);
        },
        save: function (obj) {
            var action = '/edit';
            return HttpService.post(url + action, obj);
        }
    };
}])

.service('MenuService', ['HttpService', function (HttpService) {
    var url = '/menu';
    return {
        list: function (bannerId) {
            var action = '?bannerId=' + (bannerId || '');
            return HttpService.get(url + action);
        },
        all: function () {
            var action = '/all';
            return HttpService.get(url + action);
        },
        remove: function (id) {
            var action = '/delete';
            return HttpService.post(url + action, {
                id: id
            });
        },
        byId: function (id) {
            var action = '/' + id;
            return HttpService.get(url + action);
        },
        edit: function (obj) {
            var action = '/edit';
            return HttpService.post(url + action, obj);
        },
        level: function (id) {
            var action = '/level/' + (id || 0);
            return HttpService.get(url + action);
        },
        save: function (obj) {
            var action = '/edit';
            return HttpService.post(url + action, obj);
        }
    };
}])

.service('RoleService', ['HttpService', function (HttpService) {
    var url = '/role';
    return {
        all: function () {
            var action = '';
            return HttpService.get(url + action);
        },
        byId: function (id) {
            var action = '/' + id;
            return HttpService.get(url + action);
        },
        save: function (obj) {
            var action = '/edit';
            return HttpService.post(url + action, obj);
        },
        remove: function (id) {
            var action = '/delete';
            return HttpService.post(url + action, {
                id: id
            });
        }
    };
}])

.service('UserService', ['HttpService', function (HttpService) {
    var url = '/user';
    return {
        list: function () {
            var action = '';
            return HttpService.get(url + action);
        },
        remove: function (id) {
            var action = '/delete';
            return HttpService.post(url + action, {
                id: id
            });
        },
        save: function (obj) {
            var action = '/edit';
            return HttpService.post(url + action, obj);
        },
        byId: function (id) {
            var action = '/' + id;
            return HttpService.get(url + action);
        },
        subsupplierList: function (params) {
            var action = '/subsupplierlist';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.subsupplierHost
            }, params));
        },
        modifySubsupplier: function (params) {
            var action = '/modifysub';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.subsupplierHost
            }, params));
        },
        selectUserById: function (params) {
            var action = '/getbyid';
            return HttpService.get(url + action ,angular.extend({
                host: Tools.constant.host.subsupplierHost
            }, params));
        },
        subList: function (params) {
            var action = '/sublist';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.subsupplierHost
            }, params));
        }
    };
}])

.service('WaybillService', ['HttpService', function (HttpService) {
    var url = '/waybill';
    return {
        loadWaybillCode: function () {
            var action = '/loadwaybillcode';
            return HttpService.get(url + action);
        },
        createWayBill: function (params) {
            var action = '/create';
            return HttpService.post(url + action, params);
        },
        selectBySid: function (params) {
            var action = '/selectbysid?serviceOrderId=' + params;
            return HttpService.get(url + action);
        },
        loadWayBill: function (params) {
            var action = '/list'
            return HttpService.get(url + action, params);
        },
        selectById: function (params) {
            var action = '/selectbyid?id=' + params;
            return HttpService.get(url + action);
        },
        selectWaybillContact: function (params) {
            var action = '/selectwaybillcontact';
            return HttpService.get(url + action, params);
        },
        liftAudit: function (params) {
            var action = '/liftaudit';
            return HttpService.post(url + action, params);
        },
        dealAudit: function (params) {
            var action = '/dealaudit';
            return HttpService.post(url + action, params);
        },
        proposedPrice: function (depaCode, depaAddr, destCode, destAddr, adate, serviceOrderIds) {
            var action = '/proposedprice?depaCode=' + depaCode + '&depaAddr=' + depaAddr + "&destCode=" + destCode + "&destAddr=" + destAddr + "&adate=" + adate + "&serviceOrderIds=" + serviceOrderIds + "&atime=0";
            return HttpService.get(url + action);
        },
        updatePayStatus: function (params) {
            var action = '/updatepaystatus';
            return HttpService.post(url + action, params);
        },
        createVeneerWaybill: function (params) {
            var action = '/veneercreate';
            return HttpService.post(url + action, params);
        },
        listDelivery: function (params) {
            var action = '/listDelivery';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        orderlistInfo: function (params) {
            var action = '/'+params;
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }));
        },
        checkDelivery: function (params) {
            var action = '/checkDelivery';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        listintransit: function (params) {
            var action = '/listintransit';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        openCheckPic: function (params) {
            var action = '/openCheckPic';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        closeCheckPic: function (params) {
            var action = '/closeCheckPic';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        waybilllist: function (params) {
            var action = '/waybilllist';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        }
    }
}])

.service('WayBillContactsService', ['HttpService', function (HttpService) {
    var url = '/contacts';
    return {
        selectContacts: function (params) {
            var action = '/selectcontacts';
            return HttpService.get(url + action, params);
        },
        addContact: function (params) {
            var action = '/add';
            return HttpService.post(url + action, params);
        }
    }
}])

.service('BillService', ['HttpService', function (HttpService) {
    var url = '/cbill';
    return {
        driverList: function (params) {
            var action = '/driver';
            return HttpService.get(url + action, params);
        },
        stancompanyList: function (params) {
            var action = '/stancompany';
            return HttpService.get(url + action, params);
        },
        kyleownerList: function (params) {
            var action = '/kyleowner';
            return HttpService.get(url + action, params);
        },
        kylecompanyList: function (params) {
            var action = '/kylecompany';
            return HttpService.get(url + action, params);
        },
        statement: function (params) {
            var action = '/statement';
            return HttpService.post(url + action, params);
        },
        updateStanCompanyStatus: function (params) {
            var action = '/stancompany';
            return HttpService.post(url + action, params);
        }
    }
}])

.service('BillSendbydriverService', ['HttpService', function (HttpService) {
    debugger;
    var url = '/balance';
    return {
        sendbydriverList: function (params) {
            var action = '/billlist';
            return HttpService.get(url + action, params);
        },
        settlement: function (params) {
            var action = '/settlement';
            return HttpService.post(url + action, params);
        },
        rewardandpunish: function (params) {
        var action = '/rewardandpunish';
        return HttpService.post(url + action, params);
    }
    }
}])

.service('BilldriverService', ['HttpService', function (HttpService) {
    var url = '/balance';
    return {
        driverList: function (params) {
            var action = '/billlist';
            return HttpService.get(url + action, params);
        },
        settlement: function (params) {
            var action = '/settlement';
            return HttpService.post(url + action, params);
        }
    }
}])

.service('BankCardCtrlService', ['HttpService', function (HttpService) {
    var url = '/bank';
    return {
        list: function (params) {
            var action = '/list';
            return HttpService.get(url + action, params);
        }
    }
}])

.service('BillSendByDriverDetailsService', ['HttpService', function (HttpService) {
    var url = '/balance';
    return {
        list: function (params) {
            var action = '/details';
            return HttpService.get(url + action, params);
        }
    }
}])

.service('BillDetailsService', ['HttpService', function (HttpService) {
    var url = '/details';
    return {
        list: function (params) {
            var action = '/list';
            return HttpService.get(url + action, params);
        }
    }
}])

.service('ShippingService', ['HttpService', function (HttpService) {
    var url = '/ship';
    return {
        list: function (params) {
            var action = '/list';
            return HttpService.get(url + action, params);
        },
        updateRate: function (params) {
            var action = '/updaterate';
            return HttpService.post(url + action, params);
        },
        updateSubsidy: function (params) {
            var action = '/updatesubsidy';
            return HttpService.post(url + action, params);
        }
    }
}])

.service('ServiceOrderService', ['HttpService', function (HttpService) {
    var url = '/serviceorder';
    return {
        list: function (params) {
            var action = '/list';
            return HttpService.get(url + action, params);
        },
        updateServiceStatus: function (params) {
            var action = '/updateservicestatus';
            return HttpService.post(url + action, params);
        },
        voice: function (params) {
            var action = '/voice';
            return HttpService.get(url + action, params);
        }
    }
}])

.service('ServiceVeneerOrderService', ['HttpService', function (HttpService) {
    var url = '/veneer/serviceorder';
    return {
        list: function (params) {
            var action = '/list';
            return HttpService.get(url + action, params);
        },
        updateServiceStatus: function (params) {
            var action = '/updateservicestatus';
            return HttpService.post(url + action, params);
        }
    }
}])

.service('WaybillTrailService', ['HttpService', function (HttpService) {
    var url = '/waybilltrail';
    return {
        selectByWaybillId: function (params) {
            var action = '/selectbywaybillid';
            return HttpService.get(url + action, params);
        },
        addWaybillTrail: function (params) {
            var action = '/add';
            return HttpService.post(url + action, params);
        }
    }
}])

.service('VehicleService', ['HttpService', function (HttpService) {
    var url = '/vehicle';
    return {
        list: function (params) {
            var action = '/list';
            return HttpService.get(url + action, params);
        },
        modify: function (params) {
            var action = '/modify';
            return HttpService.post(url + action, params);
        },
        loadBrand: function () {
            var action = '/brand';
            return HttpService.get(url + action);
        },
        loadVehicle: function (brandId) {
            var action = '/vehicle?brandId=' + brandId;
            return HttpService.get(url + action);
        }
    }
}])

.service('VehicleClassifyService', ['HttpService', function (HttpService) {
    var url = '/vehicle_classify';
    return {
        pageList: function(params) {
            var action = '/page_list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        list: function(params) {
            var action = '/list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        add: function(params) {
            var action = '/add';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        update: function (params) {
            var action = '/update';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        getById: function (params) {
            var action = '/getById';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        enableOrDisabled: function (params) {
            var action = '/enableOrDisabled';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        del: function (params) {
            var action = '/del';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        }
    }
}])

.service('BrandService', ['HttpService', function (HttpService) {
    var url = '/brand';
    return {
        list: function (params) {
            var action = '/list';
            return HttpService.get(url + action, params);
        },
        edit: function (params) {
            var action = '/edit';
            return HttpService.post(url + action, params);
        }
    }
}])

.service('WaybillAttachService', ['HttpService', function (HttpService) {
    var url = '/attach';
    return {
        send: function (params) {
            var action = '/send';
            return HttpService.post(url + action, params);
        },
        delivery: function (params) {
            var action = '/delivery';
            return HttpService.post(url + action, params);
        }
    }
}])

.service('IndexService', ['HttpService', function (HttpService) {
    var url = '/index';
    return {
        countAndAmount: function (params) {
            var action = '/countandamount';
            return HttpService.get(url + action, params);
        },
        schedule: function (params) {
            var action = '/schedule';
            return HttpService.get(url + action, params);
        }
    }
}])

.service('DriversService', ['HttpService', function (HttpService) {
    var url = '/driver';
    return {
        list: function (params) {
            var action = '/list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        disabledDriver: function (params) {
            var action = '/disabled';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        enabledDriver: function (params) {
            var action = '/enabled';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        modifyphone: function (params) {
            var action = '/modifyphone';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        bindOilCard: function (params) {
            var action = '/bindoilcard';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        }
    }
}])

.service('QueueService', ['HttpService', function (HttpService) {
    var url = '/tmsqueue';
    return {
        topOrder: function (params) {
            var action = '/top';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        list: function () {
            var action = '/list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }));
        },
        removeOrder: function (params) {
            var action = '/remove';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        listDriver: function () {
            var action = '/listdriver';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }));
        },
        removeDriver: function (params) {
            var action = '/removedriver';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        }
    }
}])

.service('AccidentService', ['HttpService', function (HttpService) {
    var url = '/accident';
    return {
        list: function (params) {
            var action = '/list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.adminHost
            }, params));
        },
        auditAccident: function (params) {
            var action = '/auditaccident';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.adminHost
            }, params));
        },
        listPic: function (params) {
            var action = '/listpic';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.adminHost
            }, params));
        }
    }
}])

.service('TMSWaybillService', ['HttpService', function (HttpService) {
    var url = '/tmswaybill';
    return {
        damageList: function (params) {
            var action = '/damagelist';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.adminHost
            }, params));
        },
        tmsList: function (params) {
            var action = '/tmslist';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.adminHost
            }, params));
        },
        cancel: function (params) {
            var action = '/cancel';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.adminHost
            }, params));
        },
        damageAudit: function (params) {
            var action = '/damageaudit';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.adminHost
            }, params));
        },
        listPic: function (params) {
            var action = '/listpic';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.adminHost
            }, params));
        }
    }
}])

.service('TmsAdminService', ['HttpService', function (HttpService) {
    var url = '/tmsadmin';
    return {
        aging: function (params) {
            var action = '/aging';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        }
    };
}])

.service('UserOilCardService', ['HttpService', function (HttpService) {
    var url = '/oilcard';
    return {
        list: function (params) {
            var action = '/list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        modifyStatus : function (params) {
            var action = '/modifystatus';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        }
    };
}])

.service('VersionService', ['HttpService', function (HttpService) {
    var url = '/version';
    return {
        list: function () {
            var action = '/list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.adminHost
            }, null));
        },
        modifyVersion : function (params) {
            var action = '/modify';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.adminHost
            }, params));
        },
        delVersion : function (params) {
            var action = '/delversion';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.adminHost
            }, params));
        },
        getById : function (params) {
            var action = '/getbyid';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.adminHost
            }, params));
        }
    };
}])

.service('UserRouteService', ['HttpService', function (HttpService) {
    var url = '/userrouter';
    return {
        list: function (params) {
            var action = '/list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.subsupplierHost
            }, params));
        },
        modifyRoute : function (params) {
            var action = '/modify';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.subsupplierHost
            }, params));
        },
        getById: function (params) {
            var action = '/getbyid';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.subsupplierHost
            }, params));
        },
        delRoute: function (params) {
            var action = '/delroute';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.subsupplierHost
            }, params));
        },
        loadProvince: function (params) {
            var action = '/province';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.subsupplierHost
            }, params));
        }
    };
}])

.service('StylelicenseService', ['HttpService', function (HttpService) {
    var url = '/tmsadmin';
    return {
        list: function (params) {
            var action = '/stylelicenselist';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        modify: function (params) {
            var action = '/modifydvcstylelicense';
            return HttpService.post(url + action,angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        add: function (params) {
            var action = '/adddvcstylelicense';
            return HttpService.post(url + action,angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        delete: function (params) {
            var action = '/deletedvcstylelicense';
            return HttpService.post(url + action,angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        }
    };
}])
.service('DoilpriceService', ['HttpService', function (HttpService) {
    var url = '/doilprice';
    return {
        list: function (params) {
            var action = '/list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.adminHost
            }, params));
        },
        add: function (params) {
            var action = '/add';
            return HttpService.post(url + action,angular.extend({
                host: Tools.constant.host.adminHost
            }, params));
        },
        isEnable: function (params) {
            var action = '/isenable';
            return HttpService.post(url + action,angular.extend({
                host: Tools.constant.host.adminHost
            }, params));
        },
        loadOilProducts: function (params) {
            var action = '/loadoilproducts';
            return HttpService.post(url + action,angular.extend({
                host: Tools.constant.host.adminHost
            }, params));
        }
    };
}])
.service('VcstyleoilService', ['HttpService', function (HttpService) {
    var url = '/dvcstyleoil';
    return {
        list: function (params) {
            var action = '/list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.adminHost
            }, params));
        },
        add: function (params) {
            var action = '/add';
            return HttpService.post(url + action,angular.extend({
                host: Tools.constant.host.adminHost
            }, params));
        },
        modify: function (params) {
            var action = '/modify';
            return HttpService.post(url + action,angular.extend({
                host: Tools.constant.host.adminHost
            }, params));
        }
    };
}])

.service('DLinesService', ['HttpService', function (HttpService) {
    var url = '/dlines';
    return {
        getList: function(params) {
            var action = '/list';
            return HttpService.get(url + action, params);
        },
        addDLinesList: function(params) {
            var action = '/add';
            return HttpService.post(url + action, params);
        },
        editDLinesList: function(params) {
            var action = '/modify';
            return HttpService.post(url + action, params);
        }
    }
}])

.service('UserBindDeviceService', ['HttpService', function (HttpService) {
    var url = '/binddevice';
    return {
        getList: function(params) {
            var action = '/list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        unbind: function(params) {
            var action = '/unbind';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        }
    }
}])

.service('RouteService', ['HttpService', function (HttpService) {
    var url = '/route';
    return {
        pageList: function(params) {
            var action = '/page_list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        list: function(params) {
            var action = '/list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        add: function(params) {
            var action = '/add';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        update: function (params) {
            var action = '/update';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        del: function (params) {
            var action = '/del';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        enableOrDisabled: function (params) {
            var action = '/enableOrDisabled';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        getById: function (params) {
            var action = '/getById';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        }
    }
}])

.service('CarTypeService', ['HttpService', function (HttpService) {
    var url = '/vehicle';
    return {
        pageList: function(params) {
            var action = '/page_list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        list: function(params) {
            var action = '/list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        add: function(params) {
            var action = '/add';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        update: function (params) {
            var action = '/update';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        getById: function (params) {
            var action = '/getById';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        licenseTypeList: function () {
            var action = '/licenseTypeList';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }));
        }
    }
}])

.service('FuelService', ['HttpService', function (HttpService) {
    var url = '/fuel';
    return {
        pageList: function(params) {
            var action = '/page_list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        list: function(params) {
            var action = '/list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        add: function(params) {
            var action = '/add';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        update: function (params) {
            var action = '/update';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        del: function (params) {
            var action = '/del';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        getById: function (params) {
            var action = '/getById';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        }
    }
}])

.service('RouteFreightService', ['HttpService', function (HttpService) {
    var url = '/route_freight';
    return {
        pageList: function(params) {
            var action = '/page_list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        add: function(params) {
            var action = '/add';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        del: function(params) {
            var action = '/del';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        update: function(params) {
            var action = '/update';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        getById: function(params) {
            var action = '/getById';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        }
    }
}])

.service('RouteMileService', ['HttpService', function (HttpService) {
    var url = '/route_mile';
    return {
        pageList: function(params) {
            var action = '/page_list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        add: function(params) {
            var action = '/add';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        del: function(params) {
            var action = '/del';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        update: function(params) {
            var action = '/update';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        getById: function(params) {
            var action = '/getById';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        }
    }
}])

.service('VehicleOcService', ['HttpService', function (HttpService) {
    var url = '/vehicle_oc';
    return {
        pageList: function(params) {
            var action = '/page_list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        add: function(params) {
            var action = '/add';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        del: function(params) {
            var action = '/del';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        update: function(params) {
            var action = '/update';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        getById: function(params) {
            var action = '/getById';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        }
    }
}])

.service('FuelPriceService', ['HttpService', function (HttpService) {
    var url = '/fuel_price';
    return {
        pageList: function(params) {
            var action = '/page_list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        list: function(params) {
            var action = '/list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        add: function(params) {
            var action = '/add';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        update: function(params) {
            var action = '/update';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        getById: function (params) {
            var action = '/getById';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        del: function (params) {
            var action = '/del';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        }
    }
}])

.service('VehiclePkfeService', ['HttpService', function (HttpService) {
    var url = '/vehicle_pkfe';
    return {
        list: function(params) {
            var action = '/list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        exist: function(params) {
            var action = '/exist';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        }
    }
}])

.service('WaybillLogisticsService', ['HttpService', function (HttpService) {
    var url = '/waybillLogistics';
    return {
        list: function(params) {
            var action = '/list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        }
    }
}])

.service('WaybillErrorService', ['HttpService', function (HttpService) {
    var url = '/waybillError';
    return {
        pageList: function(params) {
            var action = '/page_list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        }
    }
}])

.service('WaybillGenerateService', ['HttpService', function (HttpService) {
    var url = '/generate_result';
    return {
        pageList: function(params) {
            var action = '/page_list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        regenerate: function (params) {
            var action = '/regenerate';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        }
    }
}])
.service('WaybillAbandonedService', ['HttpService', function (HttpService) {
    var url = '/abandoned';
    return {
        pageList: function(params) {
            var action = '/page_list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        }
    }
}])

.service('CvTankvolumeService', ['HttpService', function (HttpService) {
    var url = '/cv_TankVolume';
    return {
        pageList: function(params) {
            var action = '/page_list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        list: function(params) {
            var action = '/list';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        add: function(params) {
            var action = '/add';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        update: function (params) {
            var action = '/update';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        getById: function (params) {
            var action = '/getById';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        del: function (params) {
            var action = '/del';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        }
    }
}])

.filter('trustHtml', ['$sce', function ($sce) {
    return function (input) {
        return $sce.trustAsHtml(input);
    }
}])

.service('LaborPriceImportService', ['HttpService', function (HttpService) {
    var url = '/labor_price_import';
    return {
        loadExcelData: function(params) {
            var action = '/loadExcelData';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        getById: function(params) {
            var action = '/getById';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        update: function(params) {
            var action = '/update';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        del: function(params) {
            var action = '/del';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        deleteAll: function(params) {
            var action = '/delete';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        importData: function(params) {
            var action = '/insert';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        }
    }
}])
.service('VehicleFuelImport', ['HttpService', function (HttpService) {
    var url = '/vehicle_fuel_import';
    return {
        loadExcelData: function(params) {
            var action = '/loadExcelData';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        getById: function(params) {
            var action = '/getById';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        update: function(params) {
            var action = '/update';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        del: function(params) {
            var action = '/del';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        delete: function(params) {
            var action = '/delete';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        importData: function(params) {
            var action = '/insert';
            return HttpService.post(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        }
    }
}])
.service('QuickDispatchService', ['HttpService', function (HttpService) {
            var url = '/waybill';
            return {
                listQuickDispatch: function(params) {
                    var action = '/quickDispatchList';
                    return HttpService.get(url + action, angular.extend({
                         host: Tools.constant.host.tmsdriverHost
                    }, params));
                },
                markQuickDispatchUp: function(params){
                    var action ='/markQuickDispatch';
                    return HttpService.post(url + action, angular.extend({
                        host: Tools.constant.host.tmsdriverHost
                    }, params));
                }
            }
}])
.service('QueueDriverService', ['HttpService', function (HttpService) {
    var url = '/tmsqueue';
    return {
        list: function(params) {
            var action = '/listQueueDriver';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        },
        exportExecl: function (params) {
            var action = '/exportExecl';
            return HttpService.get(url + action, angular.extend({
                host: Tools.constant.host.tmsdriverHost
            }, params));
        }
    }
}])
.service('priceQueryService',['HttpService',function(HttpService){
    var url = '/settlementPrice';
    return{
        findSettlementPrice: function(params){
            var action = '/findSettlementPrice';
            return HttpService.get(url + action,angular.extend({
                host: Tools.constant.host.tmsdriverHost
            },params))
        }
    }
}]);