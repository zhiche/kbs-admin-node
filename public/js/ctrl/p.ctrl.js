/**
 * Created by Tuffy on 16/6/6.
 */
'use strict';
adminCtrl.controller('priceQueryCtrl',['$scope','$rootScope','$timeout','priceQueryService',function($scope,$rootScope,$timeout,priceQueryService){
    //获取查询条件
    $scope.searchObj = {
        vehicleClassifyName:'',
        startAddress:'',
        endAddress:''
    };

    //获取主列表
    $scope.getList = function(){
            var start = $scope.searchObj.startAddress.title?$scope.searchObj.startAddress.title.split(' '):['',''];
            var end   = $scope.searchObj.endAddress.title?$scope.searchObj.endAddress.title.split(' '):['',''];
            var temp = {
                styleInfo:$scope.searchObj.vehicleClassifyName.title||'',
                oProvince:start[0]||'',
                oTag:start[1]||'',
                tProvince: end[0],
                tTag:end[1]
            };
            priceQueryService.findSettlementPrice(temp).success(function(data){
                if(data.success){
                    $scope.tableList = data.data;

                }  else {
                    layer.open({
                        title:'提示',
                        content:data.message
                    })
                }
            })
    };
}]);