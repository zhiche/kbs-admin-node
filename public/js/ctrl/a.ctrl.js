/**
 * Created by Tuffy on 16/6/6.
 */
'use strict';

adminCtrl

.controller('AwardRecordController', ['$rootScope', '$scope', '$state', 'AwardRecordService', function ($rootScope, $scope, $state, AwardRecordService) {
	
    $scope.page = {
        pageNo: 1,
        totalPage: 0
    };

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            loadData();
        }
    };

    // 初始化数据列表
    function loadData() {
    	AwardRecordService.list({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize
        }).success(function (result) {
            if (result.success) {
                $scope.awardRecordInfo = result.data.awards;
                $scope.page = result.data.page;
                buildPage();
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    }

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    }
    
    loadData();
}])

.controller('AccidentCtrl', ['$rootScope', '$scope', '$modal', 'AccidentService', function ($rootScope, $scope, $modal, AccidentService) {
    // 查询条件
    $scope.waybill = {
        orderCode: ''
    };

    // 数据列表
    $scope.accident = [];

    // 图片列表
    $scope.picList = [];


    var picPreviewModal = $modal({scope: $scope, title: '图片预览', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/components/tms-accident-pic-view.html', show: false});

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            $scope.loadData();
        }
    };

    // 初始化数据列表
    $scope.loadData = function (params) {
        if ('query' === params){
            $scope.userTruckObj.pageNo = 1;
            $scope.userTruckObj.pageSize = 10;
            $scope.page.pageNo = 1;
            $scope.page.totalPage = 0;
        }
        AccidentService.list({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize,
            orderCode: $scope.waybill.orderCode
        }).success(function (result) {
            if (result.success) {
                $scope.accident = result.data.accidents || [];
                angular.forEach($scope.accident, function (item) {
                    item.auditStatusText = Tools.getDAccidentStatusText(item.status);
                });
                $scope.page = result.data.page;
                buildPage();
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    };

    // 清空查询条件
    $scope.clearQuery = function () {
        $scope.waybill = {
            orderCode: ''
        };
        $scope.loadData('load');
    };

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    };

    // 审核
    $scope.auditAccident = function (id, status) {
        AccidentService.auditAccident({
            id: id,
            status: status
        }).success(function (result) {
            if (result.success) {
                $rootScope.hycadmin.toast({
                    title: '审核成功',
                    timeOut: 3000
                });
                $scope.loadData('load');
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    };

    // 图片预览
    $scope.listPic = function (waybillId) {
        AccidentService.listPic({
            waybillId: waybillId
        }).success(function (result) {
            if (result.success) {
                $scope.picList = result.data.attachVos;
                picPreviewModal.$promise.then(picPreviewModal.show);
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    };

    // 初始化数据
    $scope.loadData('load');
}])
