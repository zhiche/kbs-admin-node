/**
 * Created by Tuffy on 16/6/6.
 */
'use strict';

adminCtrl

.controller('MenuCtrl', ['$scope', 'MenuService', function ($scope, MenuService) {
    // banner对象
    $scope.menuObj = {
        list: [],
        init: function () {
            var that = this;
            MenuService.all()
                .success(function (result) {
                    if (result.success) {
                        that.list = result.data;
                    }
                })
        },
        remove: function (id) {
            var that = this;
            var index = layer.confirm('删除此菜单及其子菜单？', {
                btn: ['确定','取消'], //按钮,
                skin: 'admin-confirm-btn-class',
                title: '删除菜单'
            }, function(){
                MenuService.remove(id)
                    .success(function (result) {
                        if (result.success) {
                            that.init();
                        }
                        layer.close(index);
                    })
            }, function(){
                //
            });
        }
    };

    // 初始化
    $scope.menuObj.init();
}])

.controller('MenuEditCtrl', ['$scope', '$state', 'MenuService', 'BannerService', 'RoleService', function ($scope, $state, MenuService, BannerService, RoleService) {

    var id = $state.params.id;

    // banner对象
    $scope.menuObj = {
        levelList: [],
        bannerList: [],
        roleList: [],
        adminList: [{
            code: 'T',
            name: '是'
        }, {
            code: 'F',
            name: '否'
        }],
        info: {},
        byId: function () {
            var that = this;
            if (id) {
                MenuService.byId(id)
                    .success(function (result) {
                        if (result.success) {
                            that.info = result.data;
                        }
                    });
            }
        },
        save: function () {
            MenuService.save({
                id: $scope.menuObj.info.id,
                title: $scope.menuObj.info.title,
                icon: $scope.menuObj.info.icon,
                parentId: $scope.menuObj.info.parentId,
                bannerId: $scope.menuObj.info.bannerId,
                href: $scope.menuObj.info.href,
                sort: $scope.menuObj.info.sort,
                roleIds: ($scope.menuObj.info.roleIds || []).join()
            })
                .success(function (result) {
                    if (result.success) {
                        layer.msg('编辑成功');
                        history.go(-1);
                    } else {
                        layer.msg(result.message);
                    }
                })
        },
        level: function () {
            var that = this;
            MenuService.level(id)
                .success(function (result) {
                    if (result.success) {
                        that.levelList = result.data;
                    }
                });
        },
        banners: function () {
            var that = this;
            BannerService.all()
                .success(function (result) {
                    if (result.success) {
                        that.bannerList = result.data;
                    }
                })
        },
        remove: function (id) {
            var that = this;
            var index = layer.confirm('删除此菜单及其子菜单？', {
                btn: ['确定','取消'], //按钮,
                skin: 'admin-confirm-btn-class',
                title: '删除菜单'
            }, function(){
                MenuService.remove(id)
                    .success(function (result) {
                        if (result.success) {
                            layer.msg('删除成功');
                            history.go(-1);
                        } else {
                            layer.msg(result.message);
                        }
                        layer.close(index);
                    })
            }, function(){
                //
            });
        },
        role: function () {
            var that = this;
            RoleService.all()
                .success(function (result) {
                    if (result.success) {
                        that.roleList = result.data;
                    }
                });
        }
    };

    // 初始化
    function init() {
        $scope.menuObj.byId();
        // 层级
        $scope.menuObj.level();
        // banner
        $scope.menuObj.banners();
        // 角色
        $scope.menuObj.role();
    };

    init();
}])