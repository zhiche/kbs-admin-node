/**
 * Created by Tuffy on 16/6/6.
 */
'use strict';

angular.module('adminApp', ['ngAnimate', 'mgcrea.ngStrap', 'ui.router', 'ui.bootstrap.datetimepicker', 'angularFileUpload', 'admin.controllers', 'admin.services'])

    .constant('Settings', {
        Context: {
            path: contextPath
        }
    })

    .run(['$rootScope', '$location', '$timeout', 'Settings', 'HttpService', 'BannerService', 'MenuService', function ($rootScope, $location, $timeout, Settings, HttpService, BannerService, MenuService) {
        $rootScope.Context = Settings.Context;

        // 合单服务订单列表
        $rootScope.checkOrder = [];

        // 合单服务订单IDS
        $rootScope.sorderIds = [];

        // 合单服务订单总价
        $rootScope.sorderCost = 0;

        // 当前时间
        $rootScope.currentDate = new Date();

        // 当前星期几
        $rootScope.currentWeek = $rootScope.currentDate.format('W');

        // 查看大图
        $rootScope.image = {
            picKey: '',
            flg: false
        };

        $rootScope.tomorrow = new Date();
        $rootScope.tomorrow.setDate($rootScope.tomorrow.getDate());

        $rootScope.pageIncludeUrl = Settings.Context.path + '/templates/components/page-view.html',
        $rootScope.pageIncludeUrlScd = Settings.Context.path + '/templates/components/page-view-two.html',
            // navbar
            $rootScope.navbar = {
                left: [],
                right: [{
                    href: $rootScope.Context.path + '/login/logout',
                    title: '退出'
                }]
            };
        // 运单状态
        $rootScope.wayBillStatus = [{
            code: 10,
            name: '待接单'
        }, {
            code: 20,
            name: '已接单'
        }, {
            code: 40,
            name: '待提车'
        }, {
            code: 50,
            name: '待发车'
        }, {
            code: 60,
            name: '待交车'
        }, {
            code: 80,
            name: '已交车'
        }, {
            code: 90,
            name: '已完成'
        }, {
            code: 100,
            name: '已取消'
        }];
        // 运单支付状态
        $rootScope.wayBillPayStatus = [{
            code: 10,
            name: '未支付'
        }, {
            code: 20,
            name: '待支付'
        }, {
            code: 30,
            name: '已支付'
        }];
        // 账单状态
        $rootScope.billStatus = [{
            code: 10,
            name: '未支付'
        }, {
            code: 20,
            name: '申请结算'
        }, {
            code: 30,
            name: '已支付'
        }];
        // 账单状态
        $rootScope.selfStatus = [{
            code: 0,
            name: '异常'
        }, {
            code: 1,
            name: '通过'
        }];

        // 菜单列表
        $rootScope.menu = {
            // 选择
            dropDown: function (id, parentId) {
                var that = this;
                Tools.setMenu(id);
                angular.forEach(that.list, function (item) {
                    angular.forEach(item.list, function (sum) {
                        if (id === sum.id) {
                            sum.active = true;
                        } else {
                            sum.active = false;
                        }
                    });
                    if (item.id === id) {
                        item.active = item.active ? false : true;
                    } else {
                        item.active = false;
                    }
                });
            },
            // 缩放
            parentDropDown: function (id, dropDown) {
                var that = this;
                angular.forEach(that.list, function (item) {
                    if (id === item.id) {
                        item.dropDown = dropDown ? false : true;
                    } else {
                        item.dropDown = false;
                    }
                });
            }
        };

        // 显示大图
        $rootScope.showBigImg = function (imageURL) {
            $rootScope.image.picKey = imageURL;
            $rootScope.image.flg = true;
        };

        // 隐藏大图
        $rootScope.hideImage = function () {
            $rootScope.image.picKey = '';
            $rootScope.image.flg = false;
        };

        $rootScope.save = function () {
            var fd = new FormData();
            var file = document.querySelector('input[type=file]').files[0];
            fd.append('logo', file);
            $http({
                method: 'POST',
                url: "yoururl",
                data: fd,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity
            }).success(function (response) {
                //上传成功的操作
                $rootScope.hycadmin.toast({
                    title: '上传成功',
                    timeOut: 3000
                });
            });

        };

        // 初始化方法
        $rootScope.init = function (bannerId) {
            var thisBanner = bannerId || '';
            BannerService.list(bannerId)
                .success(function (result) {
                    if (result.success) {
                        if (result.data.length > 0) {
                            var first = '';
                            var bannerData = result.data || [];
                            if (typeof bannerId == 'undefined' || bannerId == '') {
                                bannerId = bannerData[0].id;
                            }
                            angular.forEach(bannerData, function (item) {
                                if (bannerId == item.id) {
                                    first = item;
                                }
                            });
                            first.active = true;
                            thisBanner = first.id;
                            menuList(thisBanner);
                        }
                        $rootScope.navbar.left = result.data;
                    }
                });

            if (bannerId) {
                menuList(bannerId);
            }

            // 菜单列表
            function menuList(bannerId) {
                MenuService.list(bannerId)
                    .success(function (result) {
                        if (result.success) {
                            $rootScope.menu.list = result.data;
                            showMenu();
                        }
                        //loadAudio();
                    });
            }

            // 显示菜单
            function showMenu(id, subId, isHash) {
                if ($rootScope.menu.list.length > 0) {
                    var item = $rootScope.menu.list[id || 0];
                    if (item.list.length > 0) {
                        var subItem = item.list[subId || 0];
                        subItem.active = true;
                        Tools.setMenu(subItem.id);
                        if (isHash) {
                            location.hash = subItem.href;
                        } else {
                            $location.path(subItem.href);
                        }
                    } else {
                        item.active = true;
                        Tools.setMenu(item.id);
                        if (isHash) {
                            location.hash = item.href;
                        } else {
                            $location.path(item.href);
                        }
                    }
                }
            }

            // 加载语音提示
            function loadAudio() {
                // 初始化语音提示
                $rootScope.longPolling({
                    timer: 5,
                    url: '/serviceorder/voice',
                    params: {
                        date: new Date().format('yyyy-MM-dd HH:mm')
                    },
                    method: 'GET',
                    callback: function (handler) {
                        handler.option.params.date = handler.getDate().format('yyyy-MM-dd HH:mm');
                        Tools.layer.confirm('有新客户订单,是否前去调度?', '新订单语音提示', '确定', '取消', function () {
                            var gotoHref = '/split-order';
                            for (var i = 0; i < $rootScope.menu.list.length; i++) {
                                var item = $rootScope.menu.list[i];
                                if (item.href == gotoHref) {
                                    showMenu(i, 0, true);
                                    return;
                                }
                                for (var j = 0; j < item.list.length; j++) {
                                    var subItem = item.list[j];
                                    if (subItem.href == gotoHref) {
                                        showMenu(i, j, true);
                                        return;
                                    }
                                }
                            }
                        });
                    }
                });
            }
        };

        $rootScope.hycadmin = {
            toast: function (option) {
                var that = this;
                that.toast.option = angular.extend({
                    title: '',
                    timeOut: 3000,
                    okCallback: null,
                }, option);
                that.toast.flag = true;
                $timeout(function () {
                    that.toast.flag = false;
                    if (that.toast.option.okCallback) {
                        that.toast.option.okCallback();
                    }
                }, that.toast.option.timeOut);
            },
            loading: function (option) {
                var that = this;
                that.loading.option = angular.extend({
                    title: '',
                }, option);
                that.loading.flag = true;
                that.loading.hide = function () {
                    that.loading.flag = false;
                };
            }
        };

        /**
         * 长连接
         * @param option {timer: 1, url: '请求url', params: [object], method: 'GET、POST……', callback: 'function(handler) {}'}
         */
        $rootScope.longPolling = function (option) {
            // 声明连接对象
            function lp() {
                this.date = new Date();
                this.data = {},
                    this.option = angular.extend({
                        callback: function (err, date, result) {
                        },
                        method: 'get',
                        timer: 1
                    }, option);
                // 获取连接
                this.polling();
            }

            // 播放音频
            lp.prototype.playAudio = function () {
                var audioContainer = $('#audio_container');
                if (audioContainer.length) {
                    audioContainer.find('audio').get(0).play();
                    return;
                }
                var container = $('<div id="audio_container" style="display: none;"></div>');
                var audio = $('<audio src="' + contextPath + '/resource/new_message.mp3" autoplay="true"></audio>');
                audio.appendTo(container);
                container.appendTo($('body'));
            },
                // 当前时间
                lp.prototype.getDate = function () {
                    return this.date;
                },
                // 成功
                lp.prototype.execution = function () {
                    var that = this;
                    if (that.data.success) {
                        that.playAudio();
                        that.option.callback(that);
                    }
                    // 重置数据对象
                    that.date = new Date();
                    $timeout(function () {
                        that.polling();
                    }, that.option.timer * 60 * 1000);
                },

                // 连接
                lp.prototype.polling = function () {
                    var that = this;
                    that.data = {};
                    HttpService[that.option.method.toLowerCase()].call(null, that.option.url, that.option.params)
                        .success(function (result) {
                            that.data = result;
                        })
                        .finally(function () {
                            that.execution();
                        });
                }
            // 初始化
            new lp();
        };

        //日历国际化
        moment.locale('zh-cn');

        // 初始化调用
        $rootScope.init();
    }])

    .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function ($stateProvider, $urlRouterProvider, $httpProvider) {

        $stateProvider
            .state('index', {
                url: '/index',
                templateUrl: contextPath + '/templates/index-view.html',
                controller: 'IndexCtrl'
            })
            .state('user-truck', {
                url: '/user-truck',
                templateUrl: contextPath + '/templates/user-truck-view.html',
                controller: 'UserTruckCtrl'
            })
            .state('order', {
                url: '/order',
                templateUrl: contextPath + '/templates/order-view.html',
                controller: 'OrderCtrl'
            })
            .state('order-completed', {
                url: '/order-completed',
                templateUrl: contextPath + '/templates/order-completed-view.html',
                controller: 'OrderComCtrl'
            })
            .state('query_dirstartpic', {
                url: '/query_dirstartpic/{orderId}',
                templateUrl: contextPath + '/templates/order-driver-alreaystart.html',
                controller: 'OrderPicCtrl'
            })
            .state('query_donepic', {
                url: '/query_donepic/{orderId}',
                templateUrl: contextPath + '/templates/order-done.html',
                controller: 'OrderDonepicCtrl'
            })
            .state('change-status', {
                url: '/changeStatus',
                // templateUrl: contextPath + '/templates/order-view.html',
                controller: 'OrderCtrl'
            })
            .state('changeStatus_orderDone', {
                url: '/changeStatusOrderDone',
                // templateUrl: contextPath + '/templates/order-view.html',
                controller: 'OrderCtrl'
            })
            .state('truck-detail', {
                url: '/truck-detail/{truckId}/{driverId}',
                templateUrl: contextPath + '/templates/truck-detail-view.html',
                controller: 'TruckDetailCtrl'
            })
            .state('company', {
                url: '/company',
                templateUrl: contextPath + '/templates/company-view.html',
                controller: 'CompanyCtrl'
            })
            .state('company-edit-view', {
                url: '/company-edit-view/{id}',
                templateUrl: contextPath + '/templates/company-edit-view.html',
                controller: 'CompanyEditCtrl'
            })
            .state('company-user-add', {
                url: '/company-user-add',
                templateUrl: contextPath + '/templates/company-user-add-view.html',
                controller: 'CompanyUserAddCtrl'
            })
            .state('company-user', {
                url: '/company-user',
                templateUrl: contextPath + '/templates/company-user-view.html',
                controller: 'CompanyUserCtrl'
            })
            .state('orders-list', {
                url: '/orders-list',
                templateUrl: contextPath + '/templates/orders-view.html',
                controller: 'OrdersAllController'
            })
            .state('award-record-list', {
                url: '/award-record-list',
                templateUrl: contextPath + '/templates/award-record-view.html',
                controller: 'AwardRecordController'
            })
            .state('order-detail', {
                url: '/order-detail/{orderId}',
                templateUrl: contextPath + '/templates/order-detail-view.html',
                controller: 'OrderDetailController'
            })
            .state('car-insurance-detail', {
                url: '/car-insurance-detail',
                templateUrl: contextPath + '/templates/car-insurance-detail.html',
                controller: 'CarInsuranceController'
            })
            .state('cexportCarInsurances', {
                url: '/exportCarInsurances',
                templateUrl: contextPath + '/templates/order-detail-view.html',
                controller: 'CarInsuranceController'
            })
            .state('banner', {
                url: '/banner',
                templateUrl: contextPath + '/templates/banner-view.html',
                controller: 'BannerCtrl'
            })
            .state('banner-edit', {
                url: '/banner-edit/{id}',
                templateUrl: contextPath + '/templates/banner-edit-view.html',
                controller: 'BannerEditCtrl'
            })
            .state('menu', {
                url: '/menu',
                templateUrl: contextPath + '/templates/menu-view.html',
                controller: 'MenuCtrl'
            })
            .state('menu-edit', {
                url: '/menu-edit/{id}',
                templateUrl: contextPath + '/templates/menu-edit-view.html',
                controller: 'MenuEditCtrl'
            })
            .state('split-order', {
                url: '/split-order',
                templateUrl: contextPath + '/templates/split-order-view.html',
                controller: 'SplitOrderCtrl'
            })
            .state('veneer-split-order', {
                url: '/veneer-split-order',
                templateUrl: contextPath + '/templates/veneer-split-order-view.html',
                controller: 'SplitVeneerOrderCtrl'
            })
            .state('veneer-waybill-list', {
                url: '/veneer-waybill-list',
                templateUrl: contextPath + '/templates/veneer-waybill-list-view.html',
                controller: 'VeneerWaybillCtrl'
            })
            .state('bill-driver', {
                url: '/bill-driver',
                templateUrl: contextPath + '/templates/bill-driver-view.html',
                controller: 'BillDriverCtrl'
            })
            .state('bill-stancompany', {
                url: '/bill-stancompany',
                templateUrl: contextPath + '/templates/bill-stancompany-view.html',
                controller: 'BillStanCompanyCtrl'
            })
            .state('bill-kyleowner', {
                url: '/bill-kyleowner',
                templateUrl: contextPath + '/templates/bill-kyleowner-view.html',
                controller: 'BillKyleownerCtrl'
            })
            .state('bill-kylecompany', {
                url: '/bill-kylecompany',
                templateUrl: contextPath + '/templates/bill-kylecompany-view.html',
                controller: 'BillKyleCompanyCtrl'
            })
            .state('bill-sendbydriver', {
                url: '/bill-sendbydriver',
                templateUrl: contextPath + '/templates/bill-sendbydriver-view.html',
                controller: 'BillSendByDriverCtrl'
            })
            .state('bill-dirver', {
                url: '/bill-dirver',
                templateUrl: contextPath + '/templates/bill-driver-view.html',
                controller: 'BillDriverCtrl'
            })
            .state('bill-details', {
                url: '/bill-details/{billId}',
                templateUrl: contextPath + '/templates/turnover-view.html',
                controller: 'BillDetailsCtrl'
            })
            .state('bill-sendbydriver-details', {
                url: '/bill-sendbydriver-details/{billId}',
                templateUrl: contextPath + '/templates/turnover-sendbydriver-view.html',
                controller: 'BillSendbyDriverDetailsCtrl'
            })
            .state('user_bankcard', {
                url: '/user_bankcard',
                templateUrl: contextPath + '/templates/userbank-sendbydriver-view.html',
                controller: 'BankCardCtrl'
            })
            .state('role', {
                url: '/role',
                templateUrl: contextPath + '/templates/role-view.html',
                controller: 'RoleCtrl'
            })
            .state('role-edit', {
                url: '/role-edit/{id}',
                templateUrl: contextPath + '/templates/role-edit-view.html',
                controller: 'RoleEditCtrl'
            })
            .state('user', {
                url: '/user',
                templateUrl: contextPath + '/templates/user-view.html',
                controller: 'UserCtrl'
            })
            .state('user-edit', {
                url: '/user-edit/{id}',
                templateUrl: contextPath + '/templates/user-edit-view.html',
                controller: 'UserEditCtrl'
            })
            .state('waybill-preview', {
                url: '/waybill-preview',
                templateUrl: contextPath + '/templates/waybill-preview-view.html',
                controller: 'WayBillPreviewCtrl'
            })
            .state('lift-truck-list', {
                url: '/lift-truck-list',
                templateUrl: contextPath + '/templates/lift-truck-list-view.html',
                controller: 'LiftTruckListCtrl'
            })
            .state('route-manage', {
                url: '/route-manage',
                templateUrl: contextPath + '/templates/route-manage-view.html',
                controller: 'RouteManageCtrl'
            })
            .state('basis-city-level', {
                url: '/basis-city-level',
                templateUrl: contextPath + '/templates/basis-city-level-view.html',
                controller: 'BasisCityLevelCtrl'
            })
            .state('basis-distance', {
                url: '/basis-distance',
                templateUrl: contextPath + '/templates/basis-distance-view.html',
                controller: 'BasisDistanceCtrl'
            })
            .state('basis-distance-factor', {
                url: '/basis-distance-factor',
                templateUrl: contextPath + '/templates/basis-distance-factor-view.html',
                controller: 'BasisFoctorCtrl'
            })
            .state('basis-distance-coefficient', {
                url: '/basis-distance-coefficient',
                templateUrl: contextPath + '/templates/basis-distance-coefficient-view.html',
                controller: 'BasisCoefficientCtrl'
            })
            .state('service-order-delivery', {
                url: '/service-order-delivery',
                templateUrl: contextPath + '/templates/service-order-delivery-view.html',
                controller: 'ServiceOrderDeliveryCtrl'
            })
            .state('waybill-list', {
                url: '/waybill-list',
                templateUrl: contextPath + '/templates/waybill-list-view.html',
                controller: 'WayBillListCtrl'
            })
            .state('waybill-detail', {
                url: '/waybill-detail/{id}',
                templateUrl: contextPath + '/templates/waybill-detail-view.html',
                controller: 'WayBillDetailCtrl'
            })
            .state('veneer-waybill-detail', {
                url: '/veneer-waybill-detail/{id}',
                templateUrl: contextPath + '/templates/veneer-waybill-detail-view.html',
                controller: 'WayBillDetailCtrl'
            })
            .state('vehicles-list', {
                url: '/vehicles-list',
                templateUrl: contextPath + '/templates/vehicles-list-view.html',
                controller: 'VehicleCtrl'
            })
            .state('brand-list', {
                url: '/brand-list',
                templateUrl: contextPath + '/templates/brand-list-view.html',
                controller: 'BrandCtrl'
            })
            .state('tms-aging', {
                url: '/tms-aging',
                templateUrl: contextPath + '/templates/tms/tms-aging-list-view.html',
                controller: 'TmsAgingCtrl'
            })
            .state('drivers-license', {
                url: '/drivers-license',
                templateUrl: contextPath + '/templates/drivers-list-view.html',
                controller: 'DriversCtrl'
            })
            .state('order-queue', {
                url: '/order-queue',
                templateUrl: contextPath + '/templates/queue-list-view.html',
                controller: 'QueueCtrl'
            })
            .state('tms-damage-waybill', {
                url: '/tms-damage-waybill',
                templateUrl: contextPath + '/templates/tms-damage-waybill-view.html',
                controller: 'TMSDamageWaybillCtrl'
            })
            .state('tms-waybill', {
                url: '/tms-waybill',
                templateUrl: contextPath + '/templates/tms-waybill-view.html',
                controller: 'TMSWaybillCtrl'
            })
            .state('tms-accident', {
                url: '/tms-accident',
                templateUrl: contextPath + '/templates/tms-accident-view.html',
                controller: 'AccidentCtrl'
            })
            .state('driver-queue', {
                url: '/driver-queue',
                templateUrl: contextPath + '/templates/tms-driver-queue-view.html',
                controller: 'DriverQueueCtrl'
            })
            .state('user-oilcard', {
                url: '/user-oilcard',
                templateUrl: contextPath + '/templates/user-oilcard-view.html',
                controller: 'UserOilCardCtrl'
            })
            .state('subsupplier-list', {
                url: '/subsupplier-list',
                templateUrl: contextPath + '/templates/subsuppliers/subsupplier-list-view.html',
                controller: 'SubsuppliersCtrl'
            })
            .state('subsupplier-edit', {
                url: '/subsupplier-edit/{subTitle}/{id}',
                templateUrl: contextPath + '/templates/subsuppliers/subsupplier-edit-view.html',
                controller: 'SubsuppliersEditCtrl'
            })
            .state('version-list', {
                url: '/version-list',
                templateUrl: contextPath + '/templates/version-list-view.html',
                controller: 'VersionCtrl'
            })
            .state('version-edit', {
                url: '/version-edit/{id}/{versionTitle}',
                templateUrl: contextPath + '/templates/version-edit-view.html',
                controller: 'VersionEditCtrl'
            })
            .state('user-route-list', {
                url: '/user-route-list',
                templateUrl: contextPath + '/templates/subsuppliers/user-route-list-view.html',
                controller: 'UserRouteCtrl'
            })
            .state('user-route-edit', {
                url: '/user-route-edit/{id}/{routeTitle}',
                templateUrl: contextPath + '/templates/subsuppliers/user-route-edit-view.html',
                controller: 'UserRouteEditCtrl'
            })
            .state('stylelicense-list', {
                url: '/stylelicense-list',
                templateUrl: contextPath + '/templates/stylelicense-list-view.html',
                controller: 'StylelicenseCtrl'
            })
            .state('doilprice-list', {
                url: '/doilprice-list',
                templateUrl: contextPath + '/templates/tms/tms-doilprice-list-view.html',
                controller: 'DoilpriceCtrl'
            })
            .state('vcstyleoil-list', {
                url: '/vcstyleoil-list',
                templateUrl: contextPath + '/templates/tms/tms-vcstyleoil-list-view.html',
                controller: 'VcstyleoilCtrl'
            })
            .state('tms-dlines-list', {
                url: '/tms-dlines-list',
                templateUrl: contextPath + '/templates/tms/tms-dlines-list-view.html',
                controller: 'DLinesCtrl'
            })
            .state('tms-dlines-add-list', {
                url: '/tms-dlines-add-list',
                templateUrl: contextPath + '/templates/tms/tms-dlines-add-list-view.html',
                controller: 'DLinesAddCtrl'
            })
            .state('tms-dlines-edit-list', {
                url: '/tms-dlines-edit-list/{dlinesId}',
                templateUrl: contextPath + '/templates/tms/tms-dlines-edit-list-view.html',
                controller: 'DLinesEditCtrl'
            })
            .state('user-bind-device', {
                url: '/user-bind-device',
                templateUrl: contextPath + '/templates/sendbydriver/user-bind-device-view.html',
                controller: 'UserBindDeviceCtrl'
            })
            .state('user-index', {
                url: '/user-index',
                templateUrl: contextPath + '/templates/Index-html.html'
            })
            .state('ils-route', {
                url: '/ils-route',
                templateUrl: contextPath + '/templates/ils/ils-route-list-view.html',
                controller: 'RouteCtrl'
            })
            .state('route-freight', {
                url: '/route-freight',
                templateUrl: contextPath + '/templates/ils/ils-route-freight-view.html',
                controller: 'RouteFreightCtrl'
            })
            .state('ils-fire-type', {
                url: '/ils-fire-type',
                templateUrl: contextPath + '/templates/ils/ils-fire-type-view.html',
                controller: 'FireTypeCtrl'
            })
            .state('ils-fuel-oil-change', {
                url: '/ils-fuel-oil-change',
                templateUrl: contextPath + '/templates/ils/ils-fuel-oil-change-view.html',
                controller: 'FuelOilChangeCtrl'
            })
            .state('ils-car-oil-change', {
                url: '/ils-car-oil-change',
                templateUrl: contextPath + '/templates/ils/ils-car-oil-change-view.html',
                controller: 'CarOilChangeCtrl'
            })
            .state('ils-car-type', {
                url: '/ils-car-type',
                templateUrl: contextPath + '/templates/ils/ils-car-type-view.html',
                controller: 'CarTypeCtrl'
            })
            .state('tms-order-list', {
                url: '/tms-order-list',
                templateUrl: contextPath + '/templates/tms/tms-order-list-view.html',
                controller: 'TmsOrderCtrl'
            })
            .state('tms-orderUnLock-list', {
                url: '/tms-orderUnLock-list',
                templateUrl: contextPath + '/templates/tms/tms-orderUnlock-list-view.html',
                controller: 'TmsOrderUnlockCtrl'
            })
            .state('waybill-passage', {
                url: '/waybill-passage',
                templateUrl: contextPath + '/templates/tms/tms-waybill-passage-view.html',
                controller: 'TmsWaybillPassageCtrl'
            })
            .state('vehicle-classify', {
                url: '/vehicle-classify',
                templateUrl: contextPath + '/templates/ils/ils-car-classify-view.html',
                controller: 'VehicleClassifyCtrl'
            })
            .state('waybill-error', {
                url: '/waybill-error',
                templateUrl: contextPath + '/templates/tms/tms-waybill-error-view.html',
                controller: 'WaybillErrorCtrl'
            })
            .state('route-mile', {
                url: '/route-mile',
                templateUrl: contextPath + '/templates/ils/ils-route-mile-view.html',
                controller: 'RouteMileCtrl'
            })
            .state('vehicle-oc', {
                url: '/vehicle-oc',
                templateUrl: contextPath + '/templates/ils/ils-vehicle-oc-view.html',
                controller: 'VehicleOcCtrl'
            })
            .state('waybill-generate', {
                url: '/waybill-generate',
                templateUrl: contextPath + '/templates/ils/ils-waybill-generate-view.html',
                controller: 'WaybillGenerateCtrl'
            })
            .state('waybill-abandoned', {
                url: '/waybill-abandoned',
                templateUrl: contextPath + '/templates/tms/tms-waybill-abandoned-list-view.html',
                controller: 'WaybillAbandonedCtrl'
            })
            .state('labor-price-import', {
                url: '/labor-price-import',
                templateUrl: contextPath + '/templates/ils/ils-labor-price-import-view.html',
                controller: 'LaborPriceImportCtrl'
            })
            .state('car-type-fuel-consumption-import', {
                url: '/car-type-fuel-consumption-import',
                templateUrl: contextPath + '/templates/ils/ils-car-type-fuel-consumption-import-view.html',
                controller: 'CarTypeFuelConsumptionImportCtrl'
            })
            .state('cv-tankvolume', {
                url: '/cv-tankvolume',
                templateUrl: contextPath + '/templates/tankvolume/sc-base-cv-tankvolume-view.html',
                controller: 'CvTankvolumeCtrl'
            })
            .state('quick-dispatch-list',{
                url: '/quick-dispatch-list',
                templateUrl : contextPath + '/templates/quickDispatch-list-view.html',
                controller : 'QuickDispatchListCtrl'
            })
            .state('upload-otd',{
                url: '/upload-otd',
                //templateUrl: contextPath + '/templates/ils/ils-labor-price-import-view.html',
                templateUrl: contextPath + '/templates/ils/ils-kas-uploadotdtracking.html',
                controller: 'UploadOtdTrackingCtl'
            })
            .state('upload-custom-otd',{
                url: '/upload-custom-otd',
                templateUrl: contextPath + '/templates/ils/ils-kas-upload-custom-otd.html',
                controller: 'UploadCustomOtdCtl'
            })
            .state('queue-driver',{
                url: '/queue-driver',
                templateUrl: contextPath + '/templates/ils/queue-driver-list.html',
                controller: 'QueueDriverCtrl'
            })
            .state('priceQuery',{
                url:'/priceQuery',
                templateUrl : contextPath + '/templates/priceQuery.html',
                controller:'priceQueryCtrl'
            });



        $urlRouterProvider.otherwise('/index');

        // 使angular $http post提交和jQuery一致
        $httpProvider.defaults.headers.put['Content-Type'] = 'application/x-www-form-urlencoded';
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

        // Override $http service's default transformRequest
        $httpProvider.defaults.transformRequest = [function (data) {
            /**
             * The workhorse; converts an object to x-www-form-urlencoded serialization.
             * @param {Object} obj
             * @return {String}
             */
            var param = function (obj) {
                var query = '';
                var name, value, fullSubName, subName, subValue, innerObj, i;

                for (name in obj) {
                    value = obj[name];

                    if (value instanceof Array) {
                        for (i = 0; i < value.length; ++i) {
                            subValue = value[i];
                            fullSubName = name + '[' + i + ']';
                            innerObj = {};
                            innerObj[fullSubName] = subValue;
                            query += param(innerObj) + '&';
                        }
                    } else if (value instanceof Object) {
                        for (subName in value) {
                            subValue = value[subName];
                            fullSubName = name + '[' + subName + ']';
                            innerObj = {};
                            innerObj[fullSubName] = subValue;
                            query += param(innerObj) + '&';
                        }
                    } else if (value !== undefined && value !== null) {
                        query += encodeURIComponent(name) + '='
                            + encodeURIComponent(value) + '&';
                    }
                }

                return query.length ? query.substr(0, query.length - 1) : query;
            };

            return angular.isObject(data) && String(data) !== '[object File]'
                ? param(data)
                : data;
        }];
    }])