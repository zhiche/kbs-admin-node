/**
 * Created by Tuffy on 16/6/6.
 */
'use strict';

adminCtrl

.controller('DriversCtrl', ['$rootScope', '$scope', '$modal', 'DriversService', function ($rootScope, $scope, $modal, DriversService) {

    // 查询条件
    $scope.queryDriver = {
        phone: '',
        userName: ''
    };

    // 更新对象
    $scope.updateDriverPhone = {
        sId: '',
        dId: '',
        phone: ''
    };
    // 绑定油卡
    $scope.bindingObj = {
        sId: '',
        oilCardCode: ''
    };

    // 数据列表
    $scope.driverList = [];

    // 修改手机号窗体
    var myOtherModal = $modal({scope: $scope, title: '修改手机号', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/components/driver-phone.html', show: false});

    // 油卡绑定窗体
    var iolCardModal = $modal({scope: $scope, title: '油卡绑定', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/components/oil-card.html', show: false});

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            $scope.loadData();
        }
    };

    // 初始化数据列表
    $scope.loadData = function (params) {
        if ('query' === params){
            $scope.userTruckObj.pageNo = 1;
            $scope.userTruckObj.pageSize = 10;
            $scope.page.pageNo = 1;
            $scope.page.totalPage = 0;
        }
        DriversService.list({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize,
            phone: $scope.queryDriver.phone,
            userName: $scope.queryDriver.userName
        }).success(function (result) {
            if (result.success) {
                $scope.driverList = result.data.driver;
                angular.forEach($scope.driverList, function (item) {
                    item.bindStatusText = Tools.getDUserOilCardStatusText(item.bindStatus);
                })
                $scope.page = result.data.page;
                buildPage();
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    }

    // 清空查询条件
    $scope.clearQuery = function () {
        $scope.queryDriver = {
            phone: '',
            userName: ''
        };
        $scope.loadData('load');
    }

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    }

    //启用用户
    $scope.enabledDriver = function (sId, dId) {
        DriversService.enabledDriver({
            sId: sId,
            dId: dId
        }).success(function (result) {
            if (result.success) {
                $rootScope.hycadmin.toast({
                    title: '启用成功',
                    timeOut: 3000
                });
                $scope.loadData('load');
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    };

    //禁用用户
    $scope.disabledDriver = function (sId, dId) {
        DriversService.disabledDriver({
            sId: sId,
            dId: dId
        }).success(function (result) {
            if (result.success) {
                $rootScope.hycadmin.toast({
                    title: '禁用成功',
                    timeOut: 3000
                });
                $scope.loadData('load');
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    };

    // 显示修改手机号modal
    $scope.showDriverModel = function (sId, dId, phone) {
        $scope.updateDriverPhone.sId = sId;
        $scope.updateDriverPhone.dId = dId;
        $scope.updateDriverPhone.phone = phone;
        myOtherModal.$promise.then(myOtherModal.show);
    };

    // 绑定油卡窗体
    $scope.bindOil = function (sId) {
        $scope.bindingObj.sId = sId;
        iolCardModal.$promise.then(iolCardModal.show);
    };

    // 保存绑定
    $scope.modifyOilCard = function () {
        if ($scope.bindingObj.sId == '') {
            $rootScope.hycadmin.toast({
                title: '用户不能为空',
                timeOut: 3000
            });
            return;
        } else if ($scope.bindingObj.oilCardCode == '') {
            $rootScope.hycadmin.toast({
                title: '油卡卡号不能为空',
                timeOut: 3000
            });
            return;
        }
        DriversService.bindOilCard($scope.bindingObj)
            .success(function (result) {
                if (result.success) {
                    $rootScope.hycadmin.toast({
                        title: '油卡绑定成功',
                        timeOut: 3000
                    });
                    $scope.bindingObj = {
                        sId: '',
                        oilCardCode: ''
                    };
                    iolCardModal.$promise.then(iolCardModal.hide);
                    $scope.loadData('load');
                } else {
                    $rootScope.hycadmin.toast({
                        title: result.message || '绑定异常',
                        timeOut: 3000
                    });
                }
            });
    };

    //修改用户手机
    $scope.modifyPhone = function () {
        if ($scope.updateDriverPhone.phone == '') {
            $rootScope.hycadmin.toast({
                title: '手机号不能为空',
                timeOut: 3000
            });
            return;
        } else if (!Tools.isMobileNo($scope.updateDriverPhone.phone)) {
            $rootScope.hycadmin.toast({
                title: '手机号不合法,请重新输入',
                timeOut: 3000
            });
            return;
        }
        DriversService.modifyphone($scope.updateDriverPhone)
            .success(function (result) {
                if (result.success) {
                    $rootScope.hycadmin.toast({
                        title: '手机号修改成功',
                        timeOut: 3000
                    });
                    myOtherModal.$promise.then(myOtherModal.hide);
                    $scope.loadData('load');
                } else {
                    $rootScope.hycadmin.toast({
                        title: result.message,
                        timeOut: 3000
                    });
                }
            });
    };

    // 初始化数据
    $scope.loadData('load');

}])

.controller('DLinesCtrl', ['$rootScope', '$scope', 'DLinesService', function($rootScope, $scope, DLinesService){
    $scope.page = {
        pageNo: 1,
        totalPage: 0
    };

    $scope.dLinesList = [];

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            loadData();
        }
    };

    // 初始化数据列表
    function loadData() {
        DLinesService.getList({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize
        }).success(function (result){
            if(result.success){
                $scope.dLinesList = result.data.dLinesList;
                $scope.page = result.data.page;
                buildPage();
            } else{
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    }

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    }

    loadData();
}])

.controller('DLinesAddCtrl', ['$rootScope', '$scope', '$stateParams', 'AreaService', 'DLinesService', function($rootScope, $scope, $stateParams, AreaService, DLinesService){
    // 注册对象
    $scope.dlinesObj = {
        id: null,
        fromProvince: null,
        fromCode: null,
        toProvince: null,
        toCode: null,
        isKyle: null,
        mileage: null,
        createTime: null,
        updateTime: null,
        price: null
    };

    $scope.isKyleList = [
        {code: '0', name: '否'},
        {code: '1', name:  '是'}
    ];

    $scope.proDate = [];
    //获取省份列表
    $scope.loadProvince = function () {
        var index = layer.load(1, {
            shade: [0.1, '#fff']
        });
        AreaService.loadProvince()
            .success(function (result) {
                if (result.success) {
                    $scope.proDate = result.data;
                } else {
                    layer.msg(result.message || '省份下拉框加载错误');
                }
            })
            .finally(function () {
                layer.close(index);
            });
    };

    //添加
    $scope.addDLinesList = function () {
        var index = layer.load(1, {
            shade: [0.1, '#fff']
        });
        DLinesService.addDLinesList()
            .success(function(result) {
                if(result.success) {
                    layer.msg('添加成功');
                    $state.go('tms-dlines-list');
                } else {
                    layer.msg(result.message || '加载失败');
                }
            })
            .finally(function() {
                layer.close(index);
            });
    };

//    loadProvince();
}])

.controller('DLinesEditCtrl', ['$rootScope', '$scope', '$stateParams', 'AreaService', 'DLinesService', function($rootScope, $scope, $stateParams, AreaService, DLinesService){
    var id = $state.params.id;

    $scope.isKyleList = [
        {code: '0', name: '否'},
        {code: '1', name:  '是'}
    ];

    // 注册对象
    $scope.dlinesObj = {
        id: null,
        fromProvince: null,
        fromCode: null,
        toProvince: null,
        toCode: null,
        isKyle: null,
        mileage: null,
        createTime: null,
        updateTime: null,
        price: null
    };

    // 加载省数据
    function loadProvince () {
        AreaService.loadProvince()
            .success(function (result) {
                if (result.success) {
                    $scope.proDate = result.data;
                } else {
                    $rootScope.hycadmin.toast({
                        title: result.message,
                        timeOut: 3000
                    });
                }
            });
    }

    //修改线路
    $scope.editDLinesList = function (params) {
        DLinesService.editDLinesList({
            id: $scope.dlinesObj.id,
            fromProvince: $scope.dlinesObj.fromProvince,
            toProvince: $scope.dlinesObj.toProvince,
            isKyle: $scope.dlinesObj.isKyle,
            mileage: $scope.dlinesObj.mileage,
            price: $scope.dlinesObj.price
        }).success(function (result) {
            if (result.success) {
                $state.go("tms-dlines-list");
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    };

    loadProvince();
}])
.controller('DoilpriceCtrl', ['$rootScope', '$scope','$modal', 'DoilpriceService', function ($rootScope, $scope, $modal, DoilpriceService) {

        // 查询列表
        $scope.dOilPriceList = [];

        //油品列表
        $scope.oilProductsList = [];

        // 当前对象
        $scope.userTruckObj = {
            pageNo: 1,
            pageSize: 10,
            loadMore: function ($event, pageNo) {
                this.pageNo = pageNo;
                if ((this.pageNo > $scope.page.totalPage)) {
                    $event.stopPropagation();
                    this.pageNo = $scope.page.totalPage;
                    return;
                } else if (this.pageNo < 1) {
                    $event.stopPropagation();
                    this.pageNo = 1;
                    return;
                }
                $scope.loadData();
            }
        };

        //查询对象
        $scope.dOilPriceParams={
            oilProducts:'',
            useBeginTime:'',
            useEndTime:''
        }

        // 清空查询条件
        $scope.clearQuery = function () {
            $scope.dOilPriceParams={
                oilProducts:'',
                useBeginTime:'',
                useEndTime:''
            };
            $scope.loadData('query');
        };

        // 加载油品
        $scope.loadOilProducts = function () {
            DoilpriceService.loadOilProducts()
                .success(function (result) {
                    if (result.success) {
                        $scope.oilProductsList = result.data;
                    } else {
                        $rootScope.hycadmin.toast({
                            title: result.message,
                            timeOut: 3000
                        });
                    }
                });
        };


        // 初始化数据列表
        $scope.loadData = function (params) {
            if ('query' === params){
                $scope.userTruckObj.pageNo = 1;
                $scope.userTruckObj.pageSize = 10;
            }
            if ($scope.dOilPriceParams.useBeginTime != null && $scope.dOilPriceParams.useBeginTime != '' && $scope.dOilPriceParams.useEndTime != null && $scope.dOilPriceParams.useEndTime != ''){
                if ((+$scope.dOilPriceParams.useEndTime) - (+$scope.dOilPriceParams.useBeginTime) < 0) {
                    $rootScope.hycadmin.toast({
                        title: '调度开始时间不能大于结束时间',
                        timeOut: 3000
                    });
                    return;
                }
            }
            if($scope.dOilPriceParams.useBeginTime != null && $scope.dOilPriceParams.useBeginTime != '' && typeof $scope.dOilPriceParams.useBeginTime != 'string'){
                $scope.dOilPriceParams.useBeginTime = $scope.dOilPriceParams.useBeginTime.format("yyyy-MM-dd");
            }
            if($scope.dOilPriceParams.useEndTime != null && $scope.dOilPriceParams.useEndTime != '' && typeof $scope.dOilPriceParams.useEndTime != 'string'){
                $scope.dOilPriceParams.useEndTime = $scope.dOilPriceParams.useEndTime.format("yyyy-MM-dd");
            }
            DoilpriceService.list({
                pageNo: $scope.userTruckObj.pageNo,
                pageSize: $scope.userTruckObj.pageSize,
                useBeginTime: $scope.dOilPriceParams.useBeginTime,
                useEndTime: $scope.dOilPriceParams.useEndTime,
                oilProducts:$scope.dOilPriceParams.oilProducts
            }).success(function (result) {
                if (result.success) {
                    if (result.data != null) {
                        $scope.dOilPriceList = result.data.dOilPriceList;
                    } else {
                        $scope.dOilPriceList = [];
                    }
                    $scope.page = result.data.page;
                    buildPage();
                } else {
                    $rootScope.hycadmin.toast({
                        title: result.message,
                        timeOut: 3000
                    });
                }
            })
        };

        //分页
        function buildPage () {
            $scope.item = [];
            var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
            var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
            for(var i=start; i <= end; i++){
                $scope.item.push(i);
            }
        };

        //编辑／添加－－－－－－－－－－－－－－


        //车型对象
        $scope.dOilPriceObj = {
            oilProducts: '',
            oilType: '',
            price: '',
            seal: '',
            startUseTime: ''
        };
        //定义弹出页面
        var dOilPriceModal = $modal({scope: $scope, title: '油价信息维护', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/tms/tms-doilprice-add-view.html', show: false});


        // 显示修改窗口
        $scope.showModel = function () {
            // 清空对象
            clearDOilPrice();
            dOilPriceModal.$promise.then(dOilPriceModal.show);
        };

        //设置可用不可用
        $scope.isEnable = function (id,isEnable) {
            DoilpriceService.isEnable({
                id:id,
                isEnable:!isEnable
            }).success(function (result) {
                    if (result.success) {
                        loadData();
                    }else{
                        Tools.layer.alert(result.message,"设置可用不可用");
                    }
                }).finally(function() {
                $rootScope.hycadmin.loading.hide();
            });
        }

        // 清空准驾车型对象
        function clearDOilPrice() {
            $scope.dOilPriceObj = {
                oilProducts: '',
                oilType: '',
                price: '',
                seal: '',
                startUseTime: ''
            };
        };

        //添加数据
        $scope.addDOilPrice = function () {

            if($scope.dOilPriceObj.oilProducts == null || $scope.dOilPriceObj.oilProducts == ''){
                Tools.layer.alert("油品不能为空");
                return false;
            }
            if($scope.dOilPriceObj.price == null || $scope.dOilPriceObj.price == ''){
                Tools.layer.alert("市场价不能为空");
                return false;
            }
            if($scope.dOilPriceObj.startUseTime == null || $scope.dOilPriceObj.startUseTime == ''){
                Tools.layer.alert("起效时间不能为空");
                return false;
            }
            $rootScope.hycadmin.loading({
                title: '数据处理中...'
            });
            $scope.dOilPriceObj.startUseTime = $scope.dOilPriceObj.startUseTime.format("yyyy-MM-dd");
            DoilpriceService.add($scope.dOilPriceObj)
                .success(function (result) {
                    if (result.success) {
                        dOilPriceModal.$promise.then(dOilPriceModal.hide);
                        clearDOilPrice();
                        $scope.loadData('query');
                    }else{
                        Tools.layer.alert(result.message,"添加准驾车型");
                    }
                }).finally(function() {
                $rootScope.hycadmin.loading.hide();
            });

        };

    $scope.loadData('query');

    $scope.loadOilProducts();
}])