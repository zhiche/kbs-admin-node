/**
 * Created by Tuffy on 16/6/6.
 */
'use strict';

adminCtrl

.controller('RoleCtrl', ['$rootScope', '$scope', '$state', '$modal', 'RoleService', function ($rootScope, $scope, $state, $modal, RoleService) {

    // 角色对象
    $scope.roleObj = {
        list: [],
        init: function () {
            var that = this;
            RoleService.all()
                .success(function (result) {
                    if (result.success) {
                        that.list = result.data;
                    }
                })
        },
        remove: function (id) {
            var index = layer.confirm('删除该角色？', {
                btn: ['确定','取消'], //按钮,
                skin: 'admin-confirm-btn-class',
                title: '删除角色'
            }, function(){
                RoleService.remove(id)
                    .success(function (result) {
                        if (result.success) {
                            layer.msg('删除成功');
                            history.go(-1);
                        } else {
                            layer.msg(result.message);
                        }
                        layer.close(index);
                    })
            }, function(){
                //
            });
        }
    };

    // 初始化
    $scope.roleObj.init();
}])

.controller('RoleEditCtrl', ['$scope', '$state', 'RoleService', function ($scope, $state, RoleService) {

    var id = $state.params.id;

    // 角色对象
    $scope.roleObj = {
        info: {},
        byId: function () {
            var that = this;
            if (id) {
                RoleService.byId(id)
                    .success(function (result) {
                        if (result.success) {
                            that.info = result.data;
                        }
                    })
            }
        },
        save: function () {
            RoleService.save({
                id: $scope.roleObj.info.id,
                name: $scope.roleObj.info.name
            })
                .success(function (result) {
                    if (result.success) {
                        layer.msg('编辑成功');
                        history.go(-1);
                    } else {
                        layer.msg(result.message);
                    }
                })
        }
    };

    // 初始化
    $scope.roleObj.byId();
}])

.controller('RouteManageCtrl',['$rootScope', '$scope', '$state', '$modal', 'AreaService', function ($rootScope, $scope, $state, $modal, AreaService) {
    // 查询条件
    $scope.waybillInfo = {
        departProvinceCode: null,
        departCityCode: null,
        receiptProvinceCode: null,
        receiptCityCode: null,
        beginCreateTime: null,
        endCreateTime: null,
        truckProvinceCode: null,
        truckCityCode: null
    }

    // 查询条件区域数据
    $scope.loadProvince = [];

    // 加载发车市
    $scope.departCity = [];

    // 加载到达市
    $scope.receiptCity = [];

    // 加载板车市
    $scope.truckCity = [];

    // 设置模态窗口
    var myOtherModal = $modal({scope: $scope, title: '在途信息', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/components/route-view.html', show: false});

    // 加载省数据
    function loadProvince() {
        AreaService.loadProvince()
            .success(function (result) {
                if (result.success) {
                    $scope.loadProvince = result.data;
                }
            });
    };

    // 根据省编码查询发车市
    $scope.loadDepartCity = function() {
        AreaService.loadCityByCode($scope.waybillInfo.departProvinceCode)
            .success(function (result) {
                if (result.success) {
                    $scope.departCity = result.data;
                }
            });
    };

    // 根据省编码查询收车市
    $scope.loadReceiptCity = function() {
        AreaService.loadCityByCode($scope.waybillInfo.receiptProvinceCode)
            .success(function (result) {
                if (result.success) {
                    $scope.receiptCity = result.data;
                }
            });
    };

    // 根据省编码查询板车市
    $scope.loadCity = function() {
        AreaService.loadCityByCode($scope.waybillInfo.truckProvinceCode)
            .success(function (result) {
                if (result.success) {
                    $scope.truckCity = result.data;
                }
            });
    }

    // 显示模态窗口
    $scope.showRouteModal = function () {
        myOtherModal.$promise.then(myOtherModal.show);
    }

    // 加载省数据
    loadProvince();
}])
.controller ('RouteCtrl', ['$rootScope', '$scope', '$modal', 'RouteService', 'AreaService', function ($rootScope, $scope, $modal, RouteService, AreaService) {
    // 查询条件
    $scope.routeInfo = {
        oProvinceCode: '',
        oTag: '',
        oCityCode: '',
        oCountyCode: '',
        dProvinceCode: '',
        dTag: '',
        dCityCode: '',
        dCountyCode: '',
        enable: '',
        name: ''
    };

    // 线路对象
    $scope.route = {
        id: '',
        name:  '',
        oProvinceCode: '',
        oTag: '',
        oProvince: '',
        oCityCode: '',
        oCity: '',
        oCountyCode: '',
        oCounty: '',
        oDepotCode: '',
        oDepot: '',
        oAddr: '',
        dProvinceCode: '',
        dTag: '',
        dProvince: '',
        dCityCode: '',
        dCity: '',
        dCountyCode: '',
        dCounty: '',
        dDepotCode: '',
        dDepot: '',
        dAddr: '',
        mapDistance: '',
        previousDistance: '',
        currentDistance: '',
        enable: ''
    };

    // 状态
    $scope.enables = [{
        code: false,
        name: '禁用'
    },{
        code: true,
        name: '启用'
    }];

    var addRouteModal = $modal({scope: $scope, title: '新增线路', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/ils/dialog/ils-route-add-view.html', show: false});

    var updateRouteModal = $modal({scope: $scope, title: '修改线路', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/ils/dialog/ils-route-update-view.html', show: false});

    // 查询条件区域数据
    $scope.loadProvince = [];

    // 数据列表
    $scope.routeList = [];

    // 加载省数据
    function loadProvince() {
        AreaService.loadProvince()
            .success(function (result) {
                if (result.success) {
                    $scope.loadProvince = result.data;
                }
            });
    };


    // 根据省编码查询起点市-dlg
    $scope.loadDepartCityDlg = function() {
        if ($scope.route.oProvinceCode != null) {
            angular.forEach($scope.loadProvince, function (item) {
                if (item.code == $scope.route.oProvinceCode) {
                    $scope.route.oProvince = item.name;
                }
            });
        }
    };

    // 根据省编码查询终点市-dlg
    $scope.loadReceiptCityDlg = function() {
        if ($scope.route.dProvinceCode != null) {
            angular.forEach($scope.loadProvince, function (item) {
                if (item.code == $scope.route.dProvinceCode) {
                    $scope.route.dProvince = item.name;
                }
            });
        }
    };

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            $scope.loadData();
        }
    };

    // 初始化数据列表
    $scope.loadData = function (params) {
        if ('query' === params){
            $scope.userTruckObj.pageNo = 1;
            $scope.userTruckObj.pageSize = 10;
            $scope.page.pageNo = 1;
            $scope.page.totalPage = 0;
        }
        RouteService.pageList({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize,
            oProvinceCode: $scope.routeInfo.oProvinceCode,
            oTag: $scope.routeInfo.oTag,
            dProvinceCode: $scope.routeInfo.dProvinceCode,
            dTag: $scope.routeInfo.dTag,
            enable: $scope.routeInfo.enable,
            name: $scope.routeInfo.name
        }).success(function (result) {
            if (result.success) {
                $scope.routeList = result.data.list;
                $scope.page = result.data.page;
                buildPage();
            } else {
                layer.msg(result.message || '数据加载异常');
            }
        });
    };

    // 清空查询条件
    $scope.clearQuery = function () {
        $scope.routeInfo = {
            oProvinceCode: '',
            oTag: '',
            oCityCode: '',
            oCountyCode: '',
            dProvinceCode: '',
            dTag: '',
            dCityCode: '',
            dCountyCode: '',
            enable: '',
            name: ''
        };
        $scope.loadData('query');
    };

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    };

    // 启用/禁用
    $scope.enableOrDisabled = function (item, en) {
        RouteService.enableOrDisabled({
            id: item.id,
            enableOrDisabled: en
        }).success(function(result){
            if (result.success) {
                $scope.loadData('load');
            } else {
                layer.msg(result.message || '更新数据异常');
            }
        })
    };

    // 清空文本框
    function cl() {
        $scope.route = {
            id: '',
            name:  '',
            oProvinceCode: '',
            oTag: '',
            oProvince: '',
            oCityCode: '',
            oCity: '',
            oCountyCode: '',
            oCounty: '',
            oDepotCode: '',
            oDepot: '',
            oAddr: '',
            dProvinceCode: '',
            dTag: '',
            dProvince: '',
            dCityCode: '',
            dCity: '',
            dCountyCode: '',
            dCounty: '',
            dDepotCode: '',
            dDepot: '',
            dAddr: '',
            mapDistance: '',
            previousDistance: '',
            currentDistance: '',
            enable: ''
        };
    };

    // 新增-dlg
    $scope.showAddDlg = function () {
        cl();
        addRouteModal.$promise.then(addRouteModal.show);
    };

    // 修改-dlg
    $scope.showUpdateDlg = function (item) {
        cl();
        RouteService.getById({id: item.id}).success(function(result) {
            if (result.success) {
                $scope.route = result.data;
                //loadDeCity();
                //loadReCity();
                updateRouteModal.$promise.then(updateRouteModal.show);
            } else {
                layer.msg(result.message || '加载数据异常');
            }
        })
    };

    // 新增及修改
    $scope.saveRoute = function (saveOrUpdate) {
        if (saveOrUpdate) {
            if (validate(saveOrUpdate)) {
                update();
            }
        } else {
            if (validate(saveOrUpdate)) {
                add();
            }
        }
    };

    // 新增
    function add() {
        RouteService.add($scope.route).success(function (result) {
            if (result.success) {
                $scope.loadData('load');
                addRouteModal.$promise.then(addRouteModal.hide);
            } else {
                layer.msg(result.message || '添加数据异常');
            }
        })
    };

    // 修改
    function update() {
        delete $scope.route.createTime;
        delete $scope.route.updateTime;
        RouteService.update($scope.route).success(function (result) {
            if (result.success) {
                $scope.loadData('load');
                updateRouteModal.$promise.then(updateRouteModal.hide);
            } else {
                layer.msg(result.message || '修改数据异常');
            }
        })
    };

    // 删除
    $scope.del = function (item) {
        var index = layer.confirm('确定删除该数据？', {
            btn: ['确定','取消'], //按钮,
            skin: 'admin-confirm-btn-class',
            title: '提示'
        }, function(){
            layer.close(index);
            if (item.id == '') {
                layer.msg('删除标识不能为空');
                return false;
            }
            RouteService.del({id: item.id}).success(function (result) {
                if (result.success) {
                    $scope.loadData('load');
                } else {
                    layer.msg(result.message || '删除数据异常');
                }
            });
        }, function(){
            //
        });

    };

    $scope.generateRouteName = function () {
        if ($scope.route.oTag != '' && $scope.route.dTag != '') {
            $scope.route.name = $scope.route.oTag + '-' + $scope.route.dTag + '线';
        }
    };

    // 校验
    function validate(saveOrUpdate) {
        if (saveOrUpdate) {
            if ($scope.route.id == '' || $scope.route.id == null) {
                layer.msg('主键不能为空');
                return false;
            }
        }
        if ($scope.route.oProvince == '') {
            layer.msg('起点省份不能为空');
            return false;
        } else if ($scope.route.oTag == '') {
            layer.msg('起点区域不能为空');
            return false;
        } else if (!Tools.unlawfulnessChars($scope.route.oTag)) {
            layer.msg('起点区域不能包含非法字符');
            return false;
        } else if ($scope.route.dProvince == '') {
            layer.msg('终点省份不能为空');
            return false;
        } else if ($scope.route.dTag == '') {
            layer.msg('终点区域不能为空');
            return false;
        } else if (!Tools.unlawfulnessChars($scope.route.dTag)) {
            layer.msg('终点区域不能包含非法字符');
            return false;
        } else if ($scope.route.name == '') {
            layer.msg('线路名称不能为空');
            return false;
        } else if (!Tools.unlawfulnessChars($scope.route.name)) {
            layer.msg('线路名称不能包含非法字符');
            return false;
        }
        if ($scope.route.oAddr != '') {
            if (!Tools.isAddress($scope.route.oAddr)) {
                layer.msg('起点详细地址不能包含非法字符');
                return false;
            }
        }
        if ($scope.route.dAddr != '') {
            if (!Tools.isAddress($scope.route.dAddr)) {
                layer.msg('终点详细地址不能包含非法字符');
                return false;
            }
        }
        return true;
    };

    // 加载订单列表数据
    $scope.loadData('load');
    // 加载省数据
    loadProvince();
}])


.controller('RouteFreightCtrl', ['$rootScope', '$scope', '$modal', 'RouteFreightService', 'RouteService', 'AreaService', function ($rootScope, $scope, $modal, RouteFreightService, RouteService, AreaService) {
    // 查询条件
    $scope.routeFreight = {
        oProvinceId: '',
        oTag: '',
        dProvinceId: '',
        dTag: '',
        effectiveDate: '',
        invalidDate: ''
    };

    // 线路对象
    $scope.routeFreightInfo = {
        id: '',
        routeId: '',
        currentValue: '',
        effectiveDate: ''
    };

    // 数据列表
    $scope.routeFreightList = [];

    // 线路列表
    $scope.routeList = [];

    var routeFreightAddModal = $modal({scope: $scope, title: '线路单公里价格', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/ils/dialog/route-freight-add-view.html', show: false});

    var routeFreightUpdateModal = $modal({scope: $scope, title: '线路单公里价格', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/ils/dialog/route-freight-update-view.html', show: false});

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            $scope.loadData();
        }
    };

    // 初始化数据列表
    $scope.loadData = function (params) {
        if ('query' === params){
            $scope.userTruckObj.pageNo = 1;
            $scope.userTruckObj.pageSize = 10;
            $scope.page.pageNo = 1;
            $scope.page.totalPage = 0;
        }
        RouteFreightService.pageList({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize,
            oProvinceId: $scope.routeFreight.oProvinceId,
            oTag: $scope.routeFreight.oTag,
            dProvinceId: $scope.routeFreight.dProvinceId,
            dTag: $scope.routeFreight.dTag,
            effectiveDate: $scope.routeFreight.effectiveDate == ''?"": typeof ($scope.routeFreight.effectiveDate) == 'string'?$scope.routeFreight.effectiveDate : $scope.routeFreight.effectiveDate.format("yyyy-MM-dd 00:00:00"),
            invalidDate: $scope.routeFreight.invalidDate == ''?"": typeof ($scope.routeFreight.invalidDate) == 'string'?$scope.routeFreight.invalidDate : $scope.routeFreight.invalidDate.format("yyyy-MM-dd 00:00:00")
        }).success(function (result) {
            if (result.success) {
                $scope.routeFreightList = result.data.list;
                $scope.page = result.data.page;
                buildPage();
            } else {
                layer.msg(result.message || '数据加载异常');
            }
        });
    };

    // 清空查询条件
    $scope.clearQuery = function () {
        $scope.routeFreight = {
            oProvinceId: '',
            oTag: '',
            dProvinceId: '',
            dTag: '',
            effectiveDate: '',
            invalidDate: ''
        };
        $scope.loadData('query');
    };

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    };


    // 初始化线路
    function loadRoute () {
        RouteService.list().success(function (result) {
            if (result.success) {
                $scope.routeList = result.data;
            } else {
                layer.msg(result.message || '初始化线路下拉数据异常');
            }
        });
    }

    function cl() {
        $scope.routeFreightInfo = {
            id: '',
            routeId: '',
            currentValue: '',
            effectiveDate: ''
        };
    }

    // 新增-dlg
    $scope.showAddDlg = function () {
        cl();
        routeFreightAddModal.$promise.then(routeFreightAddModal.show);
    };

    // 显示修改窗体
    $scope.showUpdateDlg = function(item) {
        cl();
        RouteFreightService.getById({
            id: item.id
        }).success(function (result) {
            if (result.success) {
                $scope.routeFreightInfo = result.data;
                routeFreightUpdateModal.$promise.then(routeFreightUpdateModal.show);
            } else {
                layer.msg(result.message || '加载数据异常');
            }
        });
    };

    // 新增
    $scope.addCarRoute = function (isUpdate) {
        if (isUpdate) {
            if (validate(isUpdate)) {
                update();
            }
        } else {
            if (validate(isUpdate)) {
                add();
            }
        }

    };

    function add() {
        $scope.routeFreightInfo.effectiveDate = $scope.routeFreightInfo.effectiveDate == ''?"": typeof ($scope.routeFreightInfo.effectiveDate) == 'string'?$scope.routeFreightInfo.effectiveDate : $scope.routeFreightInfo.effectiveDate.format("yyyy-MM-dd 00:00:00")
        RouteFreightService.add($scope.routeFreightInfo).success(function (result) {
            if (result.success) {
                $scope.loadData('load');
                routeFreightAddModal.$promise.then(routeFreightAddModal.hide);
            } else {
                layer.msg(result.message || '添加数据异常');
            }
        });
    }

    $scope.del = function (item) {
        var index = layer.confirm('确定删除该数据？', {
            btn: ['确定','取消'], //按钮,
            skin: 'admin-confirm-btn-class',
            title: '提示'
        }, function(){
            layer.close(index);
            if (item.id == '') {
                layer.msg('删除标识不能为空');
                return false;
            }
            RouteFreightService.del({id: item.id}).success(function (result) {
                if (result.success) {
                    $scope.loadData('load');
                } else {
                    layer.msg(result.message || '删除数据异常');
                }
            });
        }, function(){
            //
        });
    };

    function update() {
        delete $scope.routeFreightInfo.updateTime;
        delete $scope.routeFreightInfo.createTime;
        $scope.routeFreightInfo.effectiveDate = $scope.routeFreightInfo.effectiveDate == ''?"": typeof ($scope.routeFreightInfo.effectiveDate) == 'string'?$scope.routeFreightInfo.effectiveDate : $scope.routeFreightInfo.effectiveDate.format("yyyy-MM-dd 00:00:00");
        RouteFreightService.update($scope.routeFreightInfo).success(function (result) {
            if (result.success) {
                $scope.loadData('load');
                routeFreightUpdateModal.$promise.then(routeFreightUpdateModal.hide);
            } else {
                layer.msg(result.message || '更新数据异常');
            }
        });
    }

    // 校验
    function validate(isUpdate) {
        if (isUpdate) {
            if ($scope.routeFreightInfo.id == '') {
                layer.msg('主键不能为空');
                return false;
            }
        }
        if ($scope.routeFreightInfo.routeId == '') {
            layer.msg('线路不能为空');
            return false;
        } else if ($scope.routeFreightInfo.currentValue == '') {
            layer.msg('当前运价不能为空');
            return false;
        } else if (!Tools.isNumberAndDoubleDigit($scope.routeFreightInfo.currentValue)) {
            layer.msg('当前运价只能为数字且为正数（保留两位小数）');
            return false;
        } else if ($scope.routeFreightInfo.effectiveDate == '') {
            layer.msg('生效时间不能为空');
            return false;
        }
        return true;
    }

    // 加载省数据
    function loadProvince () {
        AreaService.loadProvince()
            .success(function (result) {
                if (result.success) {
                    $scope.provinceData = result.data;
                } else {
                    $rootScope.hycadmin.toast({
                        title: result.message,
                        timeOut: 3000
                    });
                }
            });
    }

    loadProvince();
    loadRoute();
    $scope.loadData('load');
}])

.controller('RouteMileCtrl', ['$rootScope', '$scope', '$modal', 'RouteMileService', 'RouteService', 'AreaService', function ($rootScope, $scope, $modal, RouteMileService, RouteService, AreaService) {
    // 查询条件
    $scope.routeMile = {
        oProvinceId: '',
        oTag: '',
        dProvinceId: '',
        dTag: '',
        effectiveDate: '',
        invalidDate: ''
    };

    // 线路对象
    $scope.routeMileInfo = {
        id: '',
        routeId: '',
        currentValue: '',
        effectiveDate: ''
    };

    // 数据列表
    $scope.routeMileList = [];

    // 线路列表
    $scope.routeList = [];

    var routeMileAddModal = $modal({scope: $scope, title: '线路里程', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/ils/dialog/route-mile-add-view.html', show: false});

    var routeMileUpdateModal = $modal({scope: $scope, title: '线路里程', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/ils/dialog/route-mile-update-view.html', show: false});

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            $scope.loadData();
        }
    };

    // 初始化数据列表
    $scope.loadData = function (params) {
        if ('query' === params){
            $scope.userTruckObj.pageNo = 1;
            $scope.userTruckObj.pageSize = 10;
            $scope.page.pageNo = 1;
            $scope.page.totalPage = 0;
        }
        RouteMileService.pageList({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize,
            oProvinceId: $scope.routeMile.oProvinceId,
            oTag: $scope.routeMile.oTag,
            dProvinceId: $scope.routeMile.dProvinceId,
            dTag: $scope.routeMile.dTag,
            effectiveDate: $scope.routeMile.effectiveDate == ''?"": typeof ($scope.routeMile.effectiveDate) == 'string'?$scope.routeMile.effectiveDate : $scope.routeMile.effectiveDate.format("yyyy-MM-dd 00:00:00"),
            invalidDate: $scope.routeMile.invalidDate == ''?"": typeof ($scope.routeMile.invalidDate) == 'string'?$scope.routeMile.invalidDate : $scope.routeMile.invalidDate.format("yyyy-MM-dd 00:00:00")
        }).success(function (result) {
            if (result.success) {
                $scope.routeMileList = result.data.list;
                $scope.page = result.data.page;
                buildPage();
            } else {
                layer.msg(result.message || '加载数据异常');
            }
        });
    };

    // 清空查询条件
    $scope.clearQuery = function () {
        $scope.routeMile = {
            oProvinceId: '',
            oTag: '',
            dProvinceId: '',
            dTag: '',
            effectiveDate: '',
            invalidDate: ''
        };
        $scope.loadData('query');
    };

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    };


    // 初始化线路
    function loadRoute () {
        RouteService.list().success(function (result) {
            if (result.success) {
                $scope.routeList = result.data;
            } else {
                layer.msg(result.message || '初始化线路下拉数据异常');
            }
        });
    }

    function cl() {
        $scope.routeMileInfo = {
            id: '',
            routeId: '',
            currentValue: '',
            effectiveDate: ''
        };
    }

    // 新增-dlg
    $scope.showAddDlg = function () {
        cl();
        routeMileAddModal.$promise.then(routeMileAddModal.show);
    };

    // 显示修改窗体
    $scope.showUpdateDlg = function(item) {
        cl();
        RouteMileService.getById({
            id: item.id
        }).success(function (result) {
            if (result.success) {
                $scope.routeMileInfo = result.data;
                routeMileUpdateModal.$promise.then(routeMileUpdateModal.show);
            } else {
                layer.msg(result.message || '加载数据异常');
            }
        });
    };

    // 新增
    $scope.addCarRoute = function (isUpdate) {
        if (isUpdate) {
            if (validate(isUpdate)) {
                update();
            }
        } else {
            if (validate(isUpdate)) {
                add();
            }
        }

    };

    function add() {
        $scope.routeMileInfo.effectiveDate = $scope.routeMileInfo.effectiveDate == ''?"": typeof ($scope.routeMileInfo.effectiveDate) == 'string'?$scope.routeMileInfo.effectiveDate : $scope.routeMileInfo.effectiveDate.format("yyyy-MM-dd 00:00:00");
        RouteMileService.add($scope.routeMileInfo).success(function (result) {
            if (result.success) {
                $scope.loadData('load');
                routeMileAddModal.$promise.then(routeMileAddModal.hide);
            } else {
                layer.msg(result.message || '添加数据异常');
            }
        });
    }

    $scope.del = function (item) {
        var index = layer.confirm('确定删除该数据？', {
            btn: ['确定','取消'], //按钮,
            skin: 'admin-confirm-btn-class',
            title: '提示'
        }, function(){
            layer.close(index);
            if (item.id == '') {
                layer.msg('删除标识不能为空');
                return false;
            }
            RouteMileService.del({id: item.id}).success(function (result) {
                if (result.success) {
                    $scope.loadData('load');
                } else {
                    layer.msg(result.message || '删除数据异常');
                }
            });
        }, function(){
            //
        });
    };

    function update() {
        delete $scope.routeMileInfo.updateTime;
        delete $scope.routeMileInfo.createTime;
        $scope.routeMileInfo.effectiveDate = $scope.routeMileInfo.effectiveDate == ''?"": typeof ($scope.routeMileInfo.effectiveDate) == 'string'?$scope.routeMileInfo.effectiveDate : $scope.routeMileInfo.effectiveDate.format("yyyy-MM-dd 00:00:00");
        RouteMileService.update($scope.routeMileInfo).success(function (result) {
            if (result.success) {
                $scope.loadData('load');
                routeMileUpdateModal.$promise.then(routeMileUpdateModal.hide);
            } else {
                layer.msg(result.message || '更新数据异常');
            }
        });
    }

    // 校验
    function validate(isUpdate) {
        if (isUpdate) {
            if ($scope.routeMileInfo.id == '') {
                layer.msg('主键不能为空');
                return false;
            }
        }
        if ($scope.routeMileInfo.routeId == '') {
            layer.msg('线路不能为空');
            return false;
        } else if ($scope.routeMileInfo.currentValue == '') {
            layer.msg('当前里程不能为空');
            return false;
        } else if (!Tools.isNumber($scope.routeMileInfo.currentValue)) {
            layer.msg('当前里程只能为数字且为正数');
            return false;
        } else if ($scope.routeMileInfo.effectiveDate == '') {
            layer.msg('生效时间不能为空');
            return false;
        }
        return true;
    }

    // 加载省数据
    function loadProvince () {
        AreaService.loadProvince()
            .success(function (result) {
                if (result.success) {
                    $scope.provinceData = result.data;
                } else {
                    $rootScope.hycadmin.toast({
                        title: result.message,
                        timeOut: 3000
                    });
                }
            });
    }

    loadProvince();
    loadRoute();
    $scope.loadData('load');
}])

