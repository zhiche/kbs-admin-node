/**
 * Created by Tuffy on 16/6/6.
 */
'use strict';

adminCtrl

.controller('FuelOilChangeCtrl', ['$rootScope', '$scope', '$modal', 'FuelPriceService', 'FuelService', function ($rootScope, $scope, $modal, FuelPriceService, FuelService) {
    // 查询条件
    $scope.fuelPrice = {
        fuelTypeId: '',
        effectiveDate: '',
        name: ''
    };

    // 燃油价格对象
    $scope.fuelPriceInfo = {
        id: '',
        name: '',
        fuelId: '',
        marketPrice: '',
        effectiveDate: '',
        invalidDate: ''
    };

    // 数据列表
    $scope.fuelPriceList = [];
    // 默认保存
    $scope.saveOrUpdate = true;
    // 燃油类型
    $scope.fuelTypeList = [];

    var fuelTypeModal = $modal({scope: $scope, title: '燃油价格', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/ils/dialog/ils-fuel-price-modify-view.html', show: false});

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            $scope.loadData();
        }
    };

    // 初始化数据列表
    $scope.loadData = function (params) {
        if ('query' === params){
            $scope.userTruckObj.pageNo = 1;
            $scope.userTruckObj.pageSize = 10;
            $scope.page.pageNo = 1;
            $scope.page.totalPage = 0;
        }
        FuelPriceService.pageList({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize,
            fuelTypeId: $scope.fuelPrice.fuelTypeId,
            name: $scope.fuelPrice.name,
            effectiveDate: $scope.fuelPrice.effectiveDate == ''?"": typeof ($scope.fuelPrice.effectiveDate) == 'string'?$scope.fuelPrice.effectiveDate : $scope.fuelPrice.effectiveDate.format("yyyy-MM-dd 00:00:00")
        }).success(function (result) {
            if (result.success) {
                $scope.fuelPriceList = result.data.list;
                $scope.page = result.data.page;
                buildPage();
            } else {
                layer.msg(result.message || '数据加载异常');
            }
        });
    };

    // 清空查询条件
    $scope.clearQuery = function () {
        $scope.fuelPrice = {
            fuelTypeId: '',
            name: '',
            effectiveDate: ''
        };
        $scope.loadData('query');
    };

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    };

    function cl() {
        $scope.fuelPriceInfo = {
            id: '',
            name: '',
            fuelId: '',
            marketPrice: '',
            effectiveDate: '',
            invalidDate: ''
        };
    }

    // 新增及修改
    $scope.saveFuelPriceType = function (saveOrUpdate) {
        if (saveOrUpdate) {
            if (validate()) {
                add();
            }
        } else {
            if (validate()) {
                update();
            }
        }
    };

    // 新增-dlg
    $scope.showAddDlg = function () {
        cl();
        $scope.saveOrUpdate = true;
        fuelTypeModal.$promise.then(fuelTypeModal.show);
    };

    // 新增
    function add() {
        $scope.fuelPriceInfo.effectiveDate = $scope.fuelPriceInfo.effectiveDate == ''?"": typeof ($scope.fuelPriceInfo.effectiveDate) == 'string'?$scope.fuelPriceInfo.effectiveDate : $scope.fuelPriceInfo.effectiveDate.format("yyyy-MM-dd 00:00:00")
        FuelPriceService.add($scope.fuelPriceInfo).success(function (result) {
            if (result.success) {
                $scope.loadData('load');
                fuelTypeModal.$promise.then(fuelTypeModal.hide);
            } else {
                layer.msg(result.message || '添加数据异常');
            }
        })
    };

    // 修改
    function update() {
        delete $scope.fuelPriceInfo.createTime;
        delete $scope.fuelPriceInfo.updateTime;
        $scope.fuelPriceInfo.effectiveDate = $scope.fuelPriceInfo.effectiveDate == ''?"": typeof ($scope.fuelPriceInfo.effectiveDate) == 'string'?$scope.fuelPriceInfo.effectiveDate : $scope.fuelPriceInfo.effectiveDate.format("yyyy-MM-dd 00:00:00")
        FuelPriceService.update($scope.fuelPriceInfo).success(function (result) {
            if (result.success) {
                $scope.loadData('load');
                fuelTypeModal.$promise.then(fuelTypeModal.hide);
            } else {
                layer.msg(result.message || '更新数据异常');
            }
        })
    };

    // 修改-dlg
    $scope.showUpdateDlg = function (item) {
        cl();
        FuelPriceService.getById({id: item.id}).success(function(result) {
            if (result.success) {
                $scope.fuelPriceInfo = result.data;
                $scope.saveOrUpdate = false;
                fuelTypeModal.$promise.then(fuelTypeModal.show);
            } else {
                layer.msg(result.message || '加载数据异常');
            }
        })
    };

    $scope.del = function (item) {
        var index = layer.confirm('确定删除该数据？', {
            btn: ['确定','取消'], //按钮,
            skin: 'admin-confirm-btn-class',
            title: '提示'
        }, function(){
            layer.close(index);
            if (item.id == '') {
                layer.msg('删除标识不能为空');
                return false;
            }
            FuelPriceService.del({id: item.id}).success(function (result) {
                if (result.success) {
                    $scope.loadData('load');
                } else {
                    layer.msg(result.message || '删除数据异常');
                }
            });
        }, function(){
            //
        });
    };

    // 校验
    function validate() {
        if (!$scope.saveOrUpdate) {
            if ($scope.fuelPriceInfo.id == '' || $scope.fuelPriceInfo.id == null) {
                layer.msg('主键不能为空');
                return false;
            }
        }
        if ($scope.fuelPriceInfo.fuelTypeId == '') {
            layer.msg('燃油类型不能为空');
            return false;
        } else if ($scope.fuelPriceInfo.marketPrice == '') {
            layer.msg('市场价不能为空');
            return false;
        } else if (!Tools.isNumberAndDoubleDigit($scope.fuelPriceInfo.marketPrice)) {
            layer.msg('市场价只能为数字且为正数（保留两位小数）');
            return false;
        } else if ($scope.fuelPriceInfo.effectiveDate == '') {
            layer.msg('生效时间不能为空');
            return false;
        }
        return true;
    }

    function loadFuelType() {
        FuelService.list().success(function (result) {
            if (result.success) {
                $scope.fuelTypeList = result.data;
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        })
    }
    $scope.loadData('load');
    loadFuelType();

}])

.controller('FireTypeCtrl', ['$rootScope', '$scope', '$modal', 'FuelService', function ($rootScope, $scope, $modal, FuelService) {
    // 查询条件
    $scope.fuel = {
        fuelCode: '',
        fuelName: ''
    };

    // 数据对象
    $scope.fuelInfo = {
        id: '',
        fuelCode: '',
        fuelName: ''
    };

    // 数据列表
    $scope.fuelList = [];

    // 默认保存
    $scope.saveOrUpdate = true;

    var fuelTypeModal = $modal({scope: $scope, title: '燃油类型', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/ils/dialog/ils-fuel-type-modify-view.html', show: false});

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            $scope.loadData();
        }
    };

    // 初始化数据列表
    $scope.loadData = function (params) {
        if ('query' === params){
            $scope.userTruckObj.pageNo = 1;
            $scope.userTruckObj.pageSize = 10;
            $scope.page.pageNo = 1;
            $scope.page.totalPage = 0;
        }
        FuelService.pageList({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize,
            fuelCode: $scope.fuel.fuelCode,
            fuelName: $scope.fuel.fuelName
        }).success(function (result) {
            if (result.success) {
                $scope.fuelList = result.data.list;
                $scope.page = result.data.page;
                buildPage();
            } else {
                layer.msg(result.message || '数据加载异常');
            }
        });
    };

    // 清空查询条件
    $scope.clearQuery = function () {
        $scope.fuel = {
            fuelCode: '',
            fuelName: ''
        };
        $scope.loadData('query');
    };

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    };

    function cl() {
        $scope.fuelInfo = {
            id: '',
            fuelCode: '',
            fuelName: ''
        };
    }

    // 新增及修改
    $scope.saveFuelType = function (saveOrUpdate) {
        if (saveOrUpdate) {
            if (validate()) {
                add();
            }
        } else {
            if (validate()) {
                update();
            }
        }
    };

    // 新增-dlg
    $scope.showAddDlg = function () {
        cl();
        $scope.saveOrUpdate = true;
        fuelTypeModal.$promise.then(fuelTypeModal.show);
    };

    // 新增
    function add() {
        FuelService.add($scope.fuelInfo).success(function (result) {
            if (result.success) {
                $scope.loadData('load');
                fuelTypeModal.$promise.then(fuelTypeModal.hide);
            } else {
                layer.msg(result.message || '添加数据异常');
            }
        })
    };

    // 修改
    function update() {
        delete $scope.fuelInfo.createTime;
        delete $scope.fuelInfo.updateTime;
        FuelService.update($scope.fuelInfo).success(function (result) {
            if (result.success) {
                $scope.loadData('load');
                fuelTypeModal.$promise.then(fuelTypeModal.hide);
            } else {
                layer.msg(result.message || '更新数据异常');
            }
        })
    };

    // 修改-dlg
    $scope.showUpdateDlg = function (item) {
        cl();
        FuelService.getById({id: item.id}).success(function(result) {
            if (result.success) {
                $scope.fuelInfo = result.data;
                $scope.saveOrUpdate = false;
                fuelTypeModal.$promise.then(fuelTypeModal.show);
            } else {
                layer.msg(result.message || '加载数据异常');
            }
        })
    };

    // 删除
    $scope.del = function (item) {
        var index = layer.confirm('确定删除该数据？', {
            btn: ['确定','取消'], //按钮,
            skin: 'admin-confirm-btn-class',
            title: '提示'
        }, function(){
            layer.close(index);
            if (item.id == '') {
                layer.msg('删除标识不能为空');
                return false;
            }
            FuelService.del({id: item.id}).success(function (result) {
                if (result.success) {
                    $scope.loadData('load');
                } else {
                    layer.msg(result.message || '删除数据异常');
                }
            });
        }, function(){
            //
        });

    };

    // 校验
    function validate() {
        if (!$scope.saveOrUpdate) {
            if ($scope.fuelInfo.id == '' || $scope.fuelInfo.id == null) {
                layer.msg('主键不能为空');
                return false;
            }
        }
        if ($scope.fuelInfo.fuelCode == '') {
            layer.msg('燃油类型编号不能为空');
            return false;
        } else if (!Tools.isNumberLetter($scope.fuelInfo.fuelCode)) {
            layer.msg('燃油类型编号只能输入数字或字母');
            return false;
        } else if ($scope.fuelInfo.fuelName == '') {
            layer.msg('燃油类型名称不能为空');
            return false;
        } else if (!Tools.isOilName($scope.fuelInfo.fuelName)) {
            layer.msg('燃油类型名称不能包含非法字符');
            return false;
        }
        return true;
    }
    $scope.loadData('load');

}])